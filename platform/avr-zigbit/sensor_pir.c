#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include "../../ctinMacro.h"

#define DEBUG 1
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif
#include "sensor_pir.h"


void initPIRsensor()
{
	PRINTF("%s\t%d\t init PIR\n", __FILE__, __LINE__);
	cbi(DDR(PIR_PORT), PIR_PIN);
	
}
uint8_t getPIRsensorState()
{
	return bit_is_set(PIN(PIR_PORT), PIR_PIN);
}

