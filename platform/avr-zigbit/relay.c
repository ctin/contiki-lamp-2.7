#include "contiki.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif
#include "relay.h"

#include "../../ctinMacro.h"
void relayInit(void)
{
	PRINTF("%s\t%d\tinit relay\n", __FILE__, __LINE__);
	#ifdef RELAY0_PORT
	sbi(DDR(RELAY0_PORT), RELAY0_PIN);
	cbi(PORT(RELAY0_PORT), RELAY0_PIN);
	#endif //RELAY0_PORT
	#ifdef RELAY1_PORT
	sbi(DDR(RELAY1_PORT), RELAY1_PIN);
	cbi(PORT(RELAY1_PORT), RELAY1_PIN);
	#endif //RELAY1_PORT
	#ifdef RELAY2_PORT
	sbi(DDR(RELAY2_PORT), RELAY2_PIN);
	cbi(PORT(RELAY2_PORT), RELAY2_PIN);
	#endif //RELAY2_PORT
	#ifdef RELAY3_PORT
	sbi(DDR(RELAY3_PORT), RELAY3_PIN);
	cbi(PORT(RELAY3_PORT), RELAY3_PIN);
	#endif //RELAY3_PORT
}

void setRelay(uint8_t relay, uint8_t  value)
{
	if(relay > (RELAY - 1))
		return;
	if(value)
	{
		switch(relay)
		{
		default: sbi(PORT(RELAY0_PORT), RELAY0_PIN);
		#if RELAY == 1
			cbi(PORT(LED2_PORT), LED2_PIN);
		#endif //RELAY == 1
			break;
		#if RELAY == 4
		case 1: sbi(PORT(RELAY1_PORT), RELAY1_PIN); break;
		case 2: sbi(PORT(RELAY2_PORT), RELAY2_PIN); break;
		case 3: sbi(PORT(RELAY3_PORT), RELAY3_PIN); break;
		#endif //RELAY
		}
	}
	else
	{
		switch(relay)
		{
		default: cbi(PORT(RELAY0_PORT), RELAY0_PIN);
		#if RELAY == 1
			sbi(PORT(LED2_PORT), LED2_PIN);
		#endif //RELAY == 1
			break;
		#if RELAY == 4
		case 1: cbi(PORT(RELAY1_PORT), RELAY1_PIN); break;
		case 2: cbi(PORT(RELAY2_PORT), RELAY2_PIN); break;
		case 3: cbi(PORT(RELAY3_PORT), RELAY3_PIN); break;
		#endif //RELAY
		}
	}
}

void setAllrelay(uint16_t value)
{
	uint8_t i;
	for(i = 0;  i < RELAY;  i++) {
		setRelay(i, bit_is_set(value, i));
	}
}

uint8_t getRelay(uint8_t relay)
{
	switch(relay)
	{
	default: return bit_is_set(PIN(RELAY0_PORT), RELAY0_PIN);
	#if RELAY == 4
	case 1: return bit_is_set(PIN(RELAY1_PORT), RELAY1_PIN);
	case 2: return bit_is_set(PIN(RELAY2_PORT), RELAY2_PIN);
	case 3: return bit_is_set(PIN(RELAY3_PORT), RELAY3_PIN);
	#endif //RELAY
	}
}

uint16_t getAllrelay()
{
	uint16_t value = 0;
	uint8_t i;
	for(i = 0;  i < RELAY;  i++)
	{
		if(getRelay(i))
			value |= 1 << i;
	}
	return value;
}