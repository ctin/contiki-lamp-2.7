#include "contiki.h"
#include "sicslowmac.h"
#include "radio/rf230bb/rf230bb.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>


#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/rpl/rpl-private.h"

#include "../../ctinMacro.h"

#define DEBUG 0
#if DEBUG
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

#include "telekont.h"

#ifdef SLEEP_MODE
#include "sleep.h"
#endif //SLEEP_MODE


#ifdef THL
#include "sensor_light.h"
#include "sensor_humidity_temperature.h"
#endif //THL
#ifdef WATER
#include "water.h"
#endif //WATER
#include "net/rpl/rpl-private.h"

#ifdef TWI_MODULE
#include "telekont_variables.h"
#include "i2c_master.h"
#include "access.h"
#endif //TWI_MODULE

//-----------------------------------------
//   global functions 
void setConnected(uint8_t connected);
void initModule(void);

//-------------------------------------------------
extern process_event_t sendPacketEvent;

static enum STATES {
	WAIT_BEFORE_SEND,
	WAIT_FOR_ANSWER
} state;

//TODO: должен слушать два цикла, потом спать. Время сна каждый раз в два раза больше, вплоть до 6 часов.
EEMEM uint32_t e_idleTime;
uint32_t idleTime; //меняется по запросу setIdleTime
static uint32_t sendCounter;
static uint8_t circlesCount;

static uint8_t connected;

void setIdleTime(uint32_t idleTimeTmp) 
{
	if(idleTimeTmp & 0xff000000 || idleTimeTmp < 5) //negative?
	{
		PRINTF("telekont.c, line %d:\tnegative value for idleTime: %d. Will set %d sec by default\n", __LINE__, idleTimeTmp, DEFAULT_SEND_WITHOUT_RESPONSE_TIME_SECS);
		idleTimeTmp = DEFAULT_SEND_WITHOUT_RESPONSE_TIME_SECS;
	}
	PRINTF("telekont.c, line %d:\tnew value for idleTime: %d\n", __LINE__, idleTimeTmp);
	if(idleTime != idleTimeTmp)
	{
		idleTime = idleTimeTmp;
		PRINTF("telekont.c, line %d:\twriting new value 0x%lx to EEPROM\n", __LINE__, idleTimeTmp);
		eeprom_write_dword(&e_idleTime, idleTimeTmp);
	}
}

void setConnected(uint8_t connectedTmp)
{
	connected = connectedTmp;
	//sbi(PORT(LED1_PORT),  LED1_PIN);
	switch(connectedTmp) 
	{
	case 0:
		PRINTF("telekont.c, line %d:\tentering state 'WAIT_BEFORE_SEND'\n", __LINE__);
		sbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
		cbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
	#ifdef SLEEP_MODE
		PRINTF("before\n");
		setCanSleep(CAN_SLEEP_IDLE);
		PRINTF("after\n");

	#endif //SLEEP_MODE

	break;
	case 1:
		PRINTF("telekont.c, line %d:\tentering state 'WAIT_BEFORE_SEND' after success\n", __LINE__);
		cbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
		sbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
	#ifdef SLEEP_MODE
		PRINTF("before\n");
		setCanSleep(CAN_SLEEP_SUCCESS);
		PRINTF("after\n");
	#endif //SLEEP_MODE
	break;
	default:
	break;
	}
	circlesCount = 0;
	state = WAIT_BEFORE_SEND;
	sendCounter = DEFAULT_SEND_WITHOUT_RESPONSE_TIME_SECS - 1;
}
int32_t g_initialPacketId;
 
void setWaitState()
{
	PRINTF("telekont.c, line %d:\tentering state 'WAIT_FOR_ANSWER' \n", __LINE__);
	//cbi(PORT(LED1_PORT), LED1_PIN);
	sendCounter = idleTime - 1;
	state = WAIT_FOR_ANSWER;
}

void sendPacket()
{
	static char buffer[17];
	g_initialPacketId++;
	sprintf(buffer, "echo %ld", g_initialPacketId);
	#ifdef THL
		refreshLux(1);
		_delay_ms(1);
		SHTgetData();
		_delay_ms(1);
		#endif //THL
		#ifdef WATER
		getWaterSensorData();
		#endif //WATER
	process_post(PROCESS_BROADCAST, sendPacketEvent, buffer); //вход в сон происходит по приходу ответа от сервера.
}

void checkConnection()
{	
	switch(state)
	{
	case WAIT_BEFORE_SEND: //продолжаем сладко спать
	
	#ifdef SLEEP_MODE
		PRINTF("telekont.c, line %d:\tstate = WAIT_BEFORE_SEND, sending\n", __LINE__, sendCounter);
		if(connected)
		{
			cbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
			sbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
		}
		else
		{
			sbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
			cbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
		}
		sendPacket();
		setWaitState();
	#else
		PRINTF("telekont.c, line %d:\tsecs to send packet = %d\n", __LINE__, sendCounter);
		if(sendCounter)
		{
			sendCounter--;
		}
		else
		{
			sendPacket();
			setWaitState();
		}
	#endif //SLEEP_MODE
	break;
	case WAIT_FOR_ANSWER:
		PRINTF("telekont.c, line %d:\tcircle: %d,  secs to connection lost = %d\n", __LINE__, circlesCount, sendCounter);
		if(sendCounter)
		{
			if(sendCounter == idleTime / 2 && circlesCount != CIRCLES)
			{
				PRINTF("telekont.c, line %d:\thalf of time to send remains, sending DIS\n", __LINE__);
				dis_output(NULL);
			}
			sendCounter--;
		}
		else if (circlesCount < CIRCLES)
		{
			circlesCount++;
			sendPacket();
			setWaitState();
		}
		else
			setConnected(0);
	break;
	}

}

void initModule()
{
	uint32_t val;
	sbi(DDR(LED2_ERR_PORT), LED2_ERR_PIN);
	sbi(DDR(LED2_RUN_PORT), LED2_RUN_PIN);
		
	val = eeprom_read_dword(&e_idleTime);
	PRINTF("telekont.c, line %d:\treading last EEPROM idleTime: 0x%lx\n", __LINE__, val);
	setIdleTime(val);
	
	//setConnected(0);
	connected = 0;
	sbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
	cbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
	sendPacket();
	setWaitState();
}
/*---------------------------------------------------------------------------*/
PROCESS(connection_process, "Connection process");
/*---------------------------------------------------------------------------*/

PROCESS_THREAD(connection_process, ev, data)
{
	static struct etimer periodic;
	
	PROCESS_BEGIN();
	
	PROCESS_PAUSE();
	PRINTF("Connection process started\n");
	etimer_set(&periodic, 1 * CLOCK_SECOND);
#ifdef TWI_MODULE
	consist_check();
#endif //TWI_MODULE
	while(1) {
		PROCESS_YIELD();
		if(etimer_expired(&periodic)) {
			etimer_reset(&periodic);
#ifdef TWI_MODULE
			consist_check();
#endif //TWI_MODULE		
			checkConnection();
		}
	}
	PROCESS_END();
}