#ifndef TELEKONT_VARIABLES_H_
#define TELEKONT_VARIABLES_H_

#define MAX_SLOT		16



#define	TYPE_BITS	0xe0
#define	TYPE_NOT_EXIST	0x80
#define	TYPE_EXIST	0x00
#define	TYPE_OUTPUT	0x20
#define	TYPE_INPUT	0x00
#define	TYPE_ANALOG	0x40
#define	TYPE_DISCRETE	0x00
#define	TYPE_DI		(TYPE_DISCRETE	| TYPE_INPUT)
#define	TYPE_DO		(TYPE_DISCRETE	| TYPE_OUTPUT)
#define	TYPE_AI		(TYPE_ANALOG 	| TYPE_INPUT)
#define	TYPE_AO		(TYPE_ANALOG 	| TYPE_OUTPUT)

#define	ADDR_TYPE	0x00
#define	ADDR_ID			0x01
#define	ADDR_CHANNELS		0x02
#define	ADDR_DECIMAL_POINTS 	0x03
#define	ADDR_DATA		0x04

#define    _BV16(bit)      ((uint16_t) 1 << (bit))
/*
        	//Адрес регистров данных. 
		Для дискретных: 16 бит данных, далее 16 бит достоверности.
		Для аналоговых: 16 бит данных канала + 8 бит диагностических (4 значимых) + 16 бит данных считанных с АЦП для отладки
				и так для всех каналов(24+16 бита на канал)
*/

#define	ADDR_DIAG		0x05
#define	ADDR_VERSION		0x06
#define	ADDR_CALIBRATION	0x07
#define	ADDR_UNITS		0x08
	/*старший бит в UNIT показывает, что значение со знаком*/

#define	ADDR_TIMEOUT		0x09

#define ERROR_NO_ANSWER_ADC		0x10
#define	ERROR_ADC_ETALON		0x20
#define	ERROR_RELE_MALFUNCTION		0x30

#define	ERROR_NO_EXTERNAL_POWER		0x08
#define	ERROR_SHORT_CUT			0x04
#define	ERROR_LINE_BREAK		0x02
#define	ERROR_SELF_DIAG			0x01
#define	NO_ERROR			0x00

#define	TELECONT_I2C_ADDR_MOD		0x40
#define	I2C_MOD_WRITE			(TELECONT_I2C_ADDR_MOD|0x00)
#define	I2C_MOD_READ			(TELECONT_I2C_ADDR_MOD|0x01)

#define	CHANNEL_BYTES	(2+1+4)


#define	MODULE_NOT_EXIST	0x80
#define	MODULE_IS_ANALOG	0x40
#define	MODULE_FOR_OUTPUT	0x20
#define	MODULE_IS_INVERT	0x10


#define	ERR_NO_SUCH_FUNCTION		0x01
#define	ERR_NO_SUCH_MODULE		0x02
#define	ERR_WRONG_BYTES_COUNT		0x03
#define	ERR_BROKEN_MODULE		0x04
#define	ERR_ENHANCED_REG		0x05
#define	ERR_SLOT_IS_DISCRET		0x11
#define	ERR_SLOT_IS_ANALOG		0x12
#define	ERR_SLOT_FOR_WRITE		0x13
#define	ERR_SLOT_FOR_READ		0x14
#define ERR_CONFIG_CHANGED		0x15
#define	ERR_UNREAL_CHANNEL		0x16
#define	ERR_WRONG_FORMAT		0x17
#define	ERR_NO_SUCH_SUBFUNCTION		0x20
#define	ERR_EEPROM_TIMEOUT_DATA_ERROR	0x31
#define	ERR_ENHANCED_REG_USAGE		0x40
#define	ERR_GLOBAL_ERROR		0x50

#endif //TELEKONT_VARIABLES_H_