
#include "../../ctinMacro.h"

#include <util/delay.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>
#include "i2c.h"

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

#include "sensor_light.h"

short g_lux;
#ifdef TSL2550_ENABLE_COUNT_VALUE_WITH_REJECTION_FUNCTION

void tsl2550CountValueWithRejection(unsigned char *value0,
									unsigned char *value1,
									short *countValue0, 
									short *countValue1, 
									unsigned char countToleranceCh0, 
									unsigned char countToleranceCh1,
									unsigned char numberOfReadings)
{
  	unsigned char i;
	short cv1Ch0, cv1Ch1;
	short cv2Ch0, cv2Ch1;
	long cvTotCh0, chTotCh1;

	tsl2550PowerUp();  
	tsl2550Read(value0, value1);
	tsl2550PowerDown();
	cv1Ch0 = tsl2550CountValue(*value0);
	cv1Ch1 = tsl2550CountValue(*value1);

	cvTotCh0 = cv1Ch0;
	chTotCh1 = cv1Ch1;

	for(i=1; i<numberOfReadings; i++)
	{
		tsl2550PowerUp();  
		tsl2550Read(value0, value1);
		tsl2550PowerDown();
		cv2Ch0 = tsl2550CountValue(*value0);
		cv2Ch1 = tsl2550CountValue(*value1);

		if(((cv2Ch0-cv1Ch0) <= countToleranceCh0) && ((cv2Ch1-cv1Ch1) <= countToleranceCh1))
		{
			cvTotCh0 += cv2Ch0;
			chTotCh1 += cv2Ch1;
	    }
		else
		{
			i = 1;
			cvTotCh0 = cv2Ch0;
			chTotCh1 = cv2Ch1;
		}

		cv1Ch0 = cv2Ch0;
		cv1Ch1 = cv2Ch1;

	}
	*countValue0 = (unsigned short)(cvTotCh0/countToleranceCh0);
	*countValue1 = (unsigned short)(chTotCh1/countToleranceCh1);
}

#endif // TSL2550_ENABLE_COUNT_WITH_REJECTION_VALUE_FUNCTION

#ifdef TSL2550_ENABLE_CALCULATE_LUX_FUNCTION

#define MAX_LUX_STD 1846 // standard mode max lux

// Lookup table for channel ratio (i.e. channel1 / channel0)
static unsigned char ratioLut[129] = {
    100, 100, 100, 100, 100, 100, 100, 100,
    100, 100, 100, 100, 100, 100,  99,  99,
     99,  99,  99,  99,  99,  99,  99,  99,
     99,  99,  99,  98,  98,  98,  98,  98,
     98,  98,  97,  97,  97,  97,  97,  96,
     96,  96,  96,  95,  95,  95,  94,  94,
     93,  93,  93,  92,  92,  91,  91,  90,
     89,  89,  88,  87,  87,  86,  85,  84,
     83,  82,  81,  80,  79,  78,  77,  75,
     74,  73,  71,  69,  68,  66,  64,  62,
     60,  58,  56,  54,  52,  49,  47,  44,
     42,  41,  40,  40,  39,  39,  38,  38,
     37,  37,  37,  36,  36,  36,  35,  35,
     35,  35,  34,  34,  34,  34,  33,  33,
     33,  33,  32,  32,  32,  32,  32,  31,
     31,  31,  31,  31,  30,  30,  30,  30,
     30
};

// Lookup table to convert channel values to counts
static unsigned short countLut[128] = {
       0,    1,    2,    3,    4,    5,    6,    7,
       8,    9,   10,   11,   12,   13,   14,   15,
      16,   18,   20,   22,   24,   26,   28,   30,
      32,   34,   36,   38,   40,   42,   44,   46,
      49,   53,   57,   61,   65,   69,   73,   77,
      81,   85,   89,   93,   97,  101,  105,  109,
     115,  123,  131,  139,  147,  155,  163,  171,
     179,  187,  195,  203,  211,  219,  227,  235,
     247,  263,  279,  295,  311,  327,  343,  359,
     375,  391,  407,  423,  439,  455,  471,  487,
     511,  543,  575,  607,  639,  671,  703,  735,
     767,  799,  831,  863,  895,  927,  959,  991,
    1039, 1103, 1167, 1231, 1295, 1359, 1423, 1487,
    1551, 1615, 1679, 1743, 1807, 1871, 1935, 1999,
    2095, 2223, 2351, 2479, 2607, 2735, 2863, 2991,
    3119, 3247, 3375, 3503, 3631, 3759, 3887, 4015
};

// simplified lux equation approximation using lookup tables.
// see http://www.taosinc.com/downloads/pdf/dn9b_tsl2550_Lux_Calculation.pdf
short tsl2550CalculateLux(unsigned char channel0, unsigned char channel1)
{
    // check for valid bit
    if(!(channel0 & 0x80) || !(channel1 & 0x80)){
    	return -1;
    }
    // get actual 7-bit values
    channel0 &= 0x7F;
    channel1 &= 0x7F;

    // lookup count from channel value
    unsigned short count0 = countLut[channel0]; // all light
    unsigned short count1 = countLut[channel1]; // infrared only

    // calculate ratio
    // Note: the "128" is a scaling factor
    unsigned char ratio = 128; // default

    // avoid division by zero
    // and count1 must not be greater than count0
    if ((count0) && (count1 <= count0)){
        ratio = (count1 * 128 / count0);
    }else{
        return -1;
    }

    // calculate lux
    // Note: the "256" is a scaling factor
    unsigned long lux = ((count0-count1) * ratioLut[ratio]) / 256;

    // range check lux
    if (lux > MAX_LUX_STD) lux = MAX_LUX_STD;

    return (unsigned short) lux;
}

#endif // TSL2550_ENABLE_CALCULATE_LUX_FUNCTION

// global variables
unsigned char tsl2550RxBuf[1];
unsigned char tsl2550TxBuf[1];
unsigned short tsl2550ConvTime = TSL2550_STANDARD_CONVERTION_TIME_MS;

// Functions

void tsl2550Init(void)
{
  	i2cInit();
  	i2cSetBitrate(TSL2550_BUS_FREQ_KHZ);
	tsl2550ConvTime = TSL2550_STANDARD_CONVERTION_TIME_MS;
}

void tsl2550PowerUp(void)
{
	tsl2550TxBuf[0] = TSL2550_CMD_POWERUP_OR_READ_CMD_REG;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
}

void tsl2550PowerDown(void)
{
	tsl2550TxBuf[0] = TSL2550_CMD_POWERDOWN;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
}

void tsl2550ExtMode(void)
{
  	tsl2550TxBuf[0] = TSL2550_CMD_EXTMODE;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
	tsl2550ConvTime = TSL2550_EXTENDED_CONVERTION_TIME_MS;
}

void tsl2550StdMode(void)
{
  	tsl2550TxBuf[0] = TSL2550_CMD_RESET_OR_STDMODE;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
  	tsl2550ConvTime = TSL2550_STANDARD_CONVERTION_TIME_MS;
}

void tsl2550Read(unsigned char *channel0, unsigned char *channel1)
{
    unsigned short i;
	for(i=0;i<tsl2550ConvTime;i++)
	{
	  _delay_ms(1); 
	}

	// channel 0
	tsl2550RxBuf[0] = 0;
	tsl2550TxBuf[0] = TSL2550_CMD_READCH0;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
	i2cMasterReceive(TSL2550_I2C_ADDR, 1, tsl2550RxBuf);
	*channel0 = tsl2550RxBuf[0];

	// channel 1
	tsl2550RxBuf[0] = 0;
	tsl2550TxBuf[0] = TSL2550_CMD_READCH1;
	i2cMasterSend(TSL2550_I2C_ADDR, 1, tsl2550TxBuf);
	i2cMasterReceive(TSL2550_I2C_ADDR, 1, tsl2550RxBuf);
	*channel1 = tsl2550RxBuf[0];
}

static unsigned short tsl2550CV[8] = {0,16,49,115,247,511,1039,2095}; // chord value
static unsigned char tsl2550SV[8] = {1,2,4,8,16,32,64,128}; // step value

unsigned short tsl2550CountValue(unsigned char channel)
{
	unsigned char C; // chord number (0 to 7)
	unsigned char S; // step number (0 to 15)

  	C = (channel & 0x70)>>4;
	S = (channel & 0x0f);
	return (tsl2550CV[C] + tsl2550SV[C] * S);
}

void dgetLightFunction(unsigned char ext){

  unsigned char value0;
  unsigned char value1;
  short countValue0, countValue1;
  long c0, c1;
  unsigned short ctp1000;
  short lux;
  tsl2550Init();
  // Power-up
  tsl2550PowerUp();
  if(ext)
	tsl2550ExtMode();
  else
    tsl2550StdMode();
  // read the data convertion
  tsl2550Read(&value0, &value1);
  // Power-down
  tsl2550PowerDown();

  // calcule lux
  
  lux = tsl2550CalculateLux(value0, value1);

  countValue0 = tsl2550CountValue(value0);
  countValue1 = tsl2550CountValue(value1);

   tsl2550CountValueWithRejection(&value0, 
 								 &value1, 
 								 &countValue0,  
								 &countValue1,  
 								 5, 
 								 5, 
 								 3); 

  // calcule color temperature per 1000 
  c0 = (long)countValue0;
  c1 = (long)countValue1;
  ctp1000 = (unsigned short)((c1*1000)/(c0));
  printf("%dc\n", lux);
  printf("channel0: Vis-IR, channel1: IR\n");
  printf("Value0 = 0x%x, Value1 = 0x%x\n", 
		  value0, 
		  value1);
  printf("\ntsl2550: lux = %d\n\n", lux);
  printf("\ntsl2550: color temp per 1000 = %d; lux = %d\n",
		  ctp1000,
		  lux);
  printf("Fluorescent: < 100\n");
  printf("Sunglight: 250 to 500\n");
  printf("Incandecent: > 700\n");

}

short refreshLux(unsigned char ext)
{
	unsigned char value0;
	unsigned char value1;
	PRINTF("%s\t%d\t init tsl2550\n", __FILE__, __LINE__);
	tsl2550Init();
	// Power-up
	PRINTF("%s\t%d\t power up tsl2550\n", __FILE__, __LINE__);
	tsl2550PowerUp();
	PRINTF("%s\t%d\t set tsl2550 mode %s\n", __FILE__, __LINE__, ext ? "ext" : "std");

  if(ext)
	tsl2550ExtMode();
  else
    tsl2550StdMode();
	PRINTF("%s\t%d\t read tsl2550\n", __FILE__, __LINE__);

  // read the data convertion
  tsl2550Read(&value0, &value1);
  // Power-down
  	PRINTF("%s\t%d\t power down tsl2550\n", __FILE__, __LINE__);

  tsl2550PowerDown();

  // calcule lux
    	PRINTF("%s\t%d\t calculate lux tsl2550\n", __FILE__, __LINE__);

  g_lux = tsl2550CalculateLux(value0, value1);
  return g_lux;
}
