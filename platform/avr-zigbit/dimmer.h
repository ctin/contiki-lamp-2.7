#ifndef DIMMER_H_
#define DIMMER_H_

extern void initDimmer();
extern void printDimmerData();
extern void collectDimmerCommad(char *rxBuffer, uint8_t size);
extern uint8_t getDimmerPercentage();
extern uint16_t getDimmerExpectationTime();
extern uint16_t getDimmerWaitTime();
extern void setDimmerPercentage(uint8_t val);
extern void setDimmerExpectationTime(uint16_t val);
extern void setDimmerWaitTime(uint16_t val);
#endif //DIMMER_H_