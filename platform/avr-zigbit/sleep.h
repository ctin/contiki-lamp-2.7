#ifndef SLEEP_H_
#define SLEEP_H_

#define MAXIMUM_SLEEP_TIME_BETWEEN_LISTEN		(6*3600) //6 часов. Должно на годик хватить.
#define START_SLEEP_TIME_BETWEEN_LISTEN			10//167 //секунд. Будучи удвоенной, является началом интервала ожидания связи

#define CAN_NOT_SLEEP		0
#define CAN_SLEEP_SUCCESS	1
#define CAN_SLEEP_IDLE		2

extern void initSleep();
extern void setCanSleep(uint8_t canSleep);
extern void continueSleepIfNeeded();

#endif //SLEEP_H_