#ifndef SENSOR_AIR_H_
#define SENSOR_AIR_H_

extern void initAirSensor();
extern unsigned short readAirSensor(uint8_t channel); 

#endif //SENSOR_AIR_H_
