#ifndef SENSOR_LIGHT_H_
#define SENSOR_LIGHT_H_

extern short g_lux;
// define enable functions
// uncomment to enable these functions
#define TSL2550_ENABLE_COUNT_VALUE_WITH_REJECTION_FUNCTION
#define TSL2550_ENABLE_CALCULATE_LUX_FUNCTION
// constants/macros/typdefs

// functions

#ifdef TSL2550_ENABLE_COUNT_VALUE_WITH_REJECTION_FUNCTION
//! Calculate the Count Values with rejection
void tsl2550CountValueWithRejection(unsigned char *value0,
									unsigned char *value1,
									short *countValue0, 
									short *countValue1, 
									unsigned char countToleranceCh0, 
									unsigned char countToleranceCh1,
									unsigned char numberOfReadings);
#endif

#ifdef TSL2550_ENABLE_CALCULATE_LUX_FUNCTION
//! Calculate the approximated light intensity in Lux
/// see http://www.taosinc.com/downloads/pdf/dn9b_tsl2550_Lux_Calculation.pdf
/// 	Returns:
///	Light intensity in Lux.
extern short tsl2550CalculateLux(unsigned char channel0, unsigned char channel1);
#endif

// define bus frequency I2C/SMBus in kHz (up to 100kHz)
#define TSL2550_BUS_FREQ_KHZ			100

// constants/macros/typdefs
#define TSL2550_I2C_ADDR		0x72	///< Base I2C address of TSL2550 devices

// convertion time
#define TSL2550_STANDARD_CONVERTION_TIME_MS		900		///< Standard convertion time (800ms at least)
#define TSL2550_EXTENDED_CONVERTION_TIME_MS		300		///< Extended convertion time ((800/5)ms at least)

// command register defines
#define TSL2550_CMD_POWERDOWN					0x00	///< Power-down state
#define TSL2550_CMD_POWERUP_OR_READ_CMD_REG		0x03	///< Power-up state/Read command register
#define TSL2550_CMD_EXTMODE						0x1d	///< Write command to assert extended range mode
#define TSL2550_CMD_RESET_OR_STDMODE			0x18	///< Write command to reset or return to standard range mode
#define TSL2550_CMD_READCH0						0x43	///< Read ADC channel 0
#define TSL2550_CMD_READCH1						0x83	///< Read ADC channel 1

/// The TSL 2550 contains two ADC registers (channel 0 and channel 1)
// ADC register bits
#define TSL2550_ADCREG_VALID	7	///< ADC channel data is valid. 1 indicates that the ADC has written data into the channel data register, since ADCEN was asserted into the COMMAND register.
#define TSL2550_ADCREG_C2		6	///< CHORD number bit 2
#define TSL2550_ADCREG_C1		5	///< CHORD number bit 1
#define TSL2550_ADCREG_C0		4	///< CHORD number bit 0
#define TSL2550_ADCREG_S3		3	///< STEP number bit 3
#define TSL2550_ADCREG_S2		2	///< STEP number bit 2
#define TSL2550_ADCREG_S1		1	///< STEP number bit 1
#define TSL2550_ADCREG_S0		0	///< STEP number bit 0

// functions

//! Init the TSL2550 chip.
extern void tsl2550Init(void);

//! Power-up the TSL2550 chip.
extern void tsl2550PowerUp(void);

//! Power-down the TSL2550 chip.
extern void tsl2550PowerDown(void);

//! Set Extended Mode
/// The range is extended by 5x.
extern void tsl2550ExtMode(void);

//! Set Standard Mode
/// Reset device returning to standard mode.
extern void tsl2550StdMode(void);

//! Read ADC Data Registers of TSL2550 chip and returns by reference.
extern void tsl2550Read(unsigned char *channel0, unsigned char *channel1);

//! Calculate the Count Value of channel
///	Returns:
///	Count value of the channel.
extern unsigned short tsl2550CountValue(unsigned char channel);

extern void dgetLightFunction(unsigned char ext);

extern short refreshLux(unsigned char ext);


#endif //SENSOR_LIGHT_H_