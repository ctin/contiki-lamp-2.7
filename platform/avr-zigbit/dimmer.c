
#include <util/delay.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

#include "../../ctinMacro.h"
#include "dimmer.h"

static uint16_t counters[2];
static uint16_t expectationTime = 150;
static uint16_t waitTime = 16;

static uint8_t percentage = (160 - 150 * 1.0) / 1.5;

static void setDimmerOutput(uint8_t on)
{
	if(on)
	{
		sbi(PORT(DIMMER_OUTPUT_PORT), DIMMER_OUTPUT_PIN);
	}
	else
	{
		cbi(PORT(DIMMER_OUTPUT_PORT), DIMMER_OUTPUT_PIN);
	}
}

uint8_t getDimmerPercentage()
{
	return percentage;
}

void setDimmerPercentage(uint8_t val)
{
	if(val < 0 || val > 100)
		return;
	setDimmerExpectationTime(160 - val * 1.5);
	percentage = val;
}

uint16_t getDimmerExpectationTime()
{
	return expectationTime;
}

uint16_t getDimmerWaitTime()
{
	return waitTime;
}

void setDimmerExpectationTime(uint16_t val)
{
	PRINTF("%s\t%d\t dimmer: expectation time set to %d\n", __FILE__, __LINE__, val);
	if(expectationTime == val || val & 0xf000) //минимальный порог
		return;
	if(val < 7)
		val = 7;
	expectationTime = val;
	percentage = (160 - expectationTime * 1.0) / 1.5;
	counters[0] = 0;
	counters[1] = 0;
	setDimmerOutput(0);
}

void setDimmerWaitTime(uint16_t val)
{
	PRINTF("%s\t%d\t dimmer: wait time set to %d\n", __FILE__, __LINE__, val);
	if(waitTime == val)
		return;
	waitTime = val;
	counters[0] = 0;
	counters[1] = 0;
	setDimmerOutput(0);
}

void initDimmer()
{
	sbi(DDR(DIMMER_OUTPUT_PORT), DIMMER_OUTPUT_PIN);
	cbi(DDR(DIMMER_INPUT_PORT), DIMMER_INPUT_PIN);
	
	setDimmerOutput(0);
	
	sbi(PCMSK0, PCINT6);
	sbi(PCICR, PCIE0);
	// Timer/Counter 2 initialization
	// Clock source: Crystal on TOSC1 pin
	// Clock value: PCK2
	// Mode: CTC top=OCR2A
	// OC2A output: Disconnected
	// OC2B output: Disconnected
	ASSR=0x20;
	TCCR2A=0x02;
	TCCR2B=0x01;
	TCNT2=0x00;
	OCR2A=0x00;
	OCR2B=0x00;
	TIMSK2=0x02;
	
	while(bit_is_set(ASSR, TCN2UB)) {}
	while(bit_is_set(ASSR, TCR2AUB)) {}
	while(bit_is_set(ASSR, TCR2BUB)) {}
	
	sei();
}

void printDimmerData()
{
	printf("expectationTime = %d\r\nwaitTime = %d\r\n", expectationTime, waitTime);
	printf("counter0 = %d\r\ncounter1 = %d\r\n", counters[0], counters[1]);
}

void collectDimmerCommad(char *rx_buffer, uint8_t size)
{
	uint8_t i;
	switch(rx_buffer[0])
	{
	case 'x': 
		setDimmerExpectationTime(atoi(rx_buffer + 1)); 
		break;
	case 'w': 
		setDimmerWaitTime(atoi(rx_buffer + 1));
		break;
	case 'p':
		setDimmerPercentage(atoi(rx_buffer + 1));
		break;
	}
	
	for(i = 0;  i < size;  i++)
		rx_buffer[i] = '\0';
	
}

static uint8_t inputDetected;

static void setDimmerInputDetected(uint8_t arg)
{
	if(inputDetected == arg)
		return;
	if(arg)
	{
		counters[0] = counters[1] = 0;
		setDimmerOutput(0);
	}
	inputDetected = arg;
}

ISR(PCINT0_vect)
{
	setDimmerInputDetected(bit_is_set(PIN(DIMMER_INPUT_PORT), DIMMER_INPUT_PIN));
}


ISR(TIMER2_COMPA_vect)
{	
#ifdef DEBUG
	setDimmerInputDetected(bit_is_clear(PIN(BUTTON1_PORT), BUTTON1_PIN));
#endif //DEBUG
	if(counters[0] < expectationTime)
		counters[0]++;
	else if(counters[0] == expectationTime)
	{
		counters[0]++;
		setDimmerOutput(1);
	}		
	else
	{
		if(counters[1] < waitTime)
			counters[1]++;
		else if(counters[1] == waitTime)
		{
			counters[1]++;
			setDimmerOutput(0);
		}
	}
}

