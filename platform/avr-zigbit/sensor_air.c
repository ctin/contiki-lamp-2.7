#include <util/delay.h>
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <string.h>

#include "../../ctinMacro.h"

#define DEBUG 1
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif
#include "sensor_air.h"


void initAirSensor()
{
	// Analog Comparator initialization
	// Analog Comparator: Off
	// Analog Comparator Input Capture by Timer/Counter 1: Off
	ACSR=0x80;
	ADCSRB=0x00;
	DIDR1=0x00;

	// Digital input buffers on ADC8: On, ADC9: On, ADC10: On, ADC11: On
	// ADC12: On, ADC13: On, ADC14: On, ADC15: On
	DIDR2=0x00;
	ADMUX=AIR_ADC_VREF_TYPE & 0xff;
	ADCSRA=0xA3;
	ADCSRB&=0xF8;
}

unsigned short readAirSensor(uint8_t channel)
{
	ADMUX=((channel ? AIR_ADC_CHANNEL_2 : AIR_ADC_CHANNEL_1) & 0x1f) | (AIR_ADC_VREF_TYPE & 0xff);
	if ((channel ? AIR_ADC_CHANNEL_2 : AIR_ADC_CHANNEL_1) & 0x20) ADCSRB |= 0x08;
	else ADCSRB &= 0xf7;
	// Delay needed for the stabilization of the ADC input voltage
	_delay_ms(10);
	// Start the AD conversion
	ADCSRA|=0x40;
	// Wait for the AD conversion to complete
	while ((ADCSRA & 0x10)==0);
	ADCSRA|=0x10;

	return ADCW * (channel ? 1 : AIR_FACTOR);
}
