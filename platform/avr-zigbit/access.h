#ifndef ACCESS_H_
#define ACCESS_H_

void consist_init();
void consist_check();

uint8_t GetDiskretChannels(
	uint8_t slot,
	uint8_t *Hi,
	uint8_t *Lo,
	uint8_t *Hi_check,
	uint8_t *Lo_check
);

uint8_t SetDiskretChannels(
	uint8_t slot,
	uint8_t Hi, 
	uint8_t Lo,
	uint8_t *Hi_check,
	uint8_t *Lo_check
);

uint8_t SetOneDiskretChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t Value,
	uint8_t *check
);

uint8_t GetAnalogChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t *Hi,
	uint8_t *Lo,
	uint8_t *check
);

uint8_t SetAnalogChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t Hi,
	uint8_t Lo,
	uint8_t *check
);

#define	CONSIST_SAVE_SIZE	10
#define	CONSIST_CHECK_SIZE	3
extern uint8_t telekontConsist[16][CONSIST_SAVE_SIZE];
#define DELAY_BETWEEN_REQUESTS_US	50
#define delay_between_requests()	_delay_us(DELAY_BETWEEN_REQUESTS_US)

#endif //ACCESS_H_