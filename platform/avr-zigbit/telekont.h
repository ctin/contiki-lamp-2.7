#ifndef TELEKONT_H_
#define TELEKONT_H_

#define DEFAULT_SEND_WITHOUT_RESPONSE_TIME_SECS	5 //из логических соображений. 10 секунд должно хватить чтоб быстро найти сеть
#define DEFAULT_TIME_WAIT_FOR_RESPONSE	10 //время ожидания ответа от сервера в секундах
#define CIRCLES 2 //количество попыток связаться прежде чем сделать паузу

extern void initModule();
extern void setConnected(uint8_t connected);
extern void setIdleTime(uint32_t idleTime);
extern uint32_t idleTime;
PROCESS_NAME(connection_process);

#endif //TELEKONT_H_