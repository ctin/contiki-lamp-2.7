#ifndef WATER_H_
#define WATER_H_

// Voltage Reference: AVCC pin
#define ADC_VREF_TYPE ((0<<REFS1) | (1<<REFS0) | (0<<ADLAR))

extern volatile unsigned short g_waterSensorData;


void waterSensorInit(void);
void setWaterSensorState(uint8_t on);
uint8_t getWaterSensorState();
uint16_t getWaterSensorData();


#endif //WATER_H_