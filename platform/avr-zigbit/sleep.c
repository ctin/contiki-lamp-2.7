#include "contiki.h"
#include "sicslowmac.h"
#include "radio/rf230bb/rf230bb.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>

#define DEBUG 1
#if DEBUG
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

#include "sleep.h"
#include "telekont.h"

#include "../../ctinMacro.h"

//-----------------------------------------
//   global functions 
void setCanSleep(uint8_t canSleep);
void initSleep();

//-------------------------------------------------

static enum STATES {
	LISTENING_STATE,
	ENTER_SLEEP_STATE,
	SLEEPING_STATE,
	WAKE_UP_SATE,
} state;

//TODO: должен слушать два цикла, потом спать. Время сна каждый раз в два раза больше, вплоть до 6 часов.

static uint32_t sleepCounter; 
static uint32_t sleepTimeWhileLithen=START_SLEEP_TIME_BETWEEN_LISTEN - 1; //ожидание связи

void setCanSleep(uint8_t canSleep)
{
	switch(canSleep)
	{
	case CAN_NOT_SLEEP:
		PRINTF("sleep.c, line %d:\tentering state 'WAKE_UP_SATE'\n", __LINE__);
		state = WAKE_UP_SATE;
	break;
	case CAN_SLEEP_SUCCESS:
		PRINTF("sleep.c, line %d:\tentering state 'ENTER_SLEEP_STATE' after success\n", __LINE__);
		state = ENTER_SLEEP_STATE;
		sleepCounter = idleTime - 1;
		sleepTimeWhileLithen = START_SLEEP_TIME_BETWEEN_LISTEN - 1; //обнуляем этот счетчик
	break;
	case CAN_SLEEP_IDLE:
		PRINTF("sleep.c, line %d:\tentering state 'ENTER_SLEEP_STATE' for idle\n", __LINE__);
		state = ENTER_SLEEP_STATE;
		sleepCounter = sleepTimeWhileLithen - 1;
		PRINTF("sleep.c, line %d:\tsleepCounter:%ld\tdefaultVal:%d\n", __LINE__, sleepTimeWhileLithen, START_SLEEP_TIME_BETWEEN_LISTEN);
		if(sleepTimeWhileLithen * 2 < MAXIMUM_SLEEP_TIME_BETWEEN_LISTEN)
			sleepTimeWhileLithen *= 2;
	break;
	}
	continueSleepIfNeeded();
}

static void initTimer2(uint8_t on)
{
	PRINTF("sleep.c, line %d:\tinit timer: %s\n", __LINE__, on ? "on" : "off");
	if(on)
	{
		// Timer/Counter 2 initialization
		// Clock source: Crystal on TOSC1 pin
		// Clock value: PCK2/128
		// Mode: Normal top=0xFF
		// OC2A output: Disconnected
		// OC2B output: Disconnected
		ASSR=(0<<EXCLK) | (1<<AS2);
		TCCR2A=(0<<COM2A1) | (0<<COM2A0) | (0<<COM2B1) | (0<<COM2B0) | (0<<WGM21) | (0<<WGM20);
		TCCR2B=(0<<WGM22) | (1<<CS22) | (0<<CS21) | (1<<CS20);
		TCNT2=0x00;
		OCR2A=0x00;
		OCR2B=0x00;
		
		// Timer/Counter 2 Interrupt(s) initialization
		TIMSK2=(0<<OCIE2B) | (0<<OCIE2A) | (1<<TOIE2);
	}
	else
	{
		// Timer/Counter 2 initialization
		// Clock source: System Clock
		// Clock value: Timer2 Stopped
		// Mode: Normal top=0xFF
		// OC2A output: Disconnected
		// OC2B output: Disconnected
		ASSR=(0<<EXCLK) | (0<<AS2);
		TCCR2A=(0<<COM2A1) | (0<<COM2A0) | (0<<COM2B1) | (0<<COM2B0) | (0<<WGM21) | (0<<WGM20);
		TCCR2B=(0<<WGM22) | (0<<CS22) | (0<<CS21) | (0<<CS20);
		TCNT2=0x00;
		OCR2A=0x00;
		OCR2B=0x00;

		// Timer/Counter 2 Interrupt(s) initialization
		TIMSK2=(0<<OCIE2B) | (0<<OCIE2A) | (0<<TOIE2);
	}
	
	while(bit_is_set(ASSR, TCN2UB)) {}
	while(bit_is_set(ASSR, TCR2AUB)) {}
	while(bit_is_set(ASSR, TCR2BUB)) {}
	
	sei();
}

void initSleep()
{
	setCanSleep(CAN_NOT_SLEEP);
	initTimer2(1);
}

static void goToDown()
{
#ifdef LED1_PORT
	sbi(PORT(LED1_PORT), LED1_PIN);
#endif //LED1_PORT
#ifdef LED2_PORT
	sbi(PORT(LED2_PORT), LED2_PIN);
#endif //LED2_PORT
#ifdef LED3_PORT
	sbi(PORT(LED3_PORT), LED3_PIN);
#endif //LED3_PORT
#ifdef TELEKONT
	sbi(PORT(LED1_PORT), LED1_PIN);
	cbi(PORT(LED2_RUN_PORT), LED2_RUN_PIN);
	cbi(PORT(LED2_ERR_PORT), LED2_ERR_PIN);
#endif //TELEKONT
	NETSTACK_CONF_RDC.off(0);
	_delay_us(50);
	if(hal_subregister_read(SR_TRX_STATUS) == P_ON)
	{
		PRINTF("sleep.c, line %d:\tradio still p_on. Set it to off\n", __LINE__);
		hal_subregister_write(SR_TRX_CMD, CMD_FORCE_TRX_OFF);
	}
	_delay_us(500);
	set_sleep_mode(SLEEP_MODE_PWR_SAVE);
	sei();
	sleep_mode();
}

void continueSleepIfNeeded()
{	
	switch(state)
	{
	case ENTER_SLEEP_STATE: //entering sleep cycle
		state = SLEEPING_STATE;
		PRINTF("sleep.c, line %d:\tentering state 'SLEEPING_STATE'\n", __LINE__);
		_delay_ms(1);
		goToDown();	
	break;
	case SLEEPING_STATE: //продолжаем сладко спать
		PRINTF("sleep.c, line %d:\tsecs to wake up = %d\n", __LINE__, sleepCounter);
		sleepCounter--;
		if(!sleepCounter)
		{
			setCanSleep(CAN_NOT_SLEEP);
			PRINTF("sleep.c, line %d:\tentering state 'WAKE_UP_SATE'\n", __LINE__);
		}
		_delay_ms(1);
		set_sleep_mode(SLEEP_MODE_PWR_SAVE);
		sei();
		sleep_mode();
	break;
	case WAKE_UP_SATE:
		PRINTF("sleep.c, line %d:\twake up!\n", __LINE__);
		state = LISTENING_STATE;
		NETSTACK_CONF_RDC.on();
		_delay_us(500);
	break;
	case LISTENING_STATE:
		//PRINTF("sleep.c line %d:\tlistening\n", __LINE__);
	break;
	}

}

ISR(TIMER2_OVF_vect)
{
	continueSleepIfNeeded();
}