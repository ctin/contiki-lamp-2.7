#include "contiki.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif
#include "water.h"
# define MUX5    3

#include "../../ctinMacro.h"

volatile unsigned short g_waterSensorData;
static uint8_t waterSensorState;
// Read the AD conversion result
uint16_t getWaterSensorData()
{
	setWaterSensorState(1);
	PRINTF("%s\t%d\twater sensor read start\n", __FILE__, __LINE__);
	ADMUX=(WATER_ADC & 0x1f) | (ADC_VREF_TYPE & 0xff);
	if (WATER_ADC & 0x20) ADCSRB |= 0x08;
	else ADCSRB &= 0xf7;
	// Delay needed for the stabilization of the ADC input voltage
	_delay_ms(10);
	// Start the AD conversion
	ADCSRA|=0x40;
	// Wait for the AD conversion to complete
	_delay_ms(100);
	PRINTF("%s\t%d\twait for AD conversion complete\n", __FILE__, __LINE__);
	while ((ADCSRA & 0x10)==0);
	ADCSRA|=0x10;
	g_waterSensorData = ADCW;
	setWaterSensorState(0);
	PRINTF("%s\t%d\twater sensor read result = %d\n", __FILE__, __LINE__, g_waterSensorData);
	return g_waterSensorData;
}

void setWaterSensorState(uint8_t on)
{
	PRINTF("%s\t%d\twater sensor state will set to %x\n", __FILE__, __LINE__, on);
	waterSensorState = on;
	if(on)
	{
		PRINTF("%s\t%d\tstarting water sensor\n", __FILE__, __LINE__);
		cbi(PORT(WATER_PORT), WATER_PIN);
		sbi(ADCSRA, ADEN);
	}
	else
	{
		PRINTF("%s\t%d\tstopping water sensor\n", __FILE__, __LINE__);
		sbi(PORT(WATER_PORT), WATER_PIN);
		cbi(ADCSRA, ADEN);
	}
	_delay_ms(10);
}

uint8_t getWaterSensorState()
{
	return waterSensorState;
}



void waterSensorInit(void)
{
	PRINTF("%s\t%d\tinit water sensor\n", __FILE__, __LINE__);
	sbi(DDR(WATER_PORT), WATER_PIN);
	ADMUX=ADC_VREF_TYPE & 0xff;
	ADCSRA=0xA3;
	ADCSRB&=0xF8;
	setWaterSensorState(1);
}