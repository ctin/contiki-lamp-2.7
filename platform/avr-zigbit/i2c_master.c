#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include<avr/interrupt.h>
#include<util/crc16.h>
#include "../../ctinMacro.h"
#include "telekont_variables.h"
#include "i2c_master.h"

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

static volatile uint8_t I2C_status;

static uint8_t I2C_node,I2C_addr,I2C_reading,*I2C_buff,I2C_size;

static uint8_t I2C_len_R;
static uint8_t I2C_len_W;

void I2C_init(void)
{
//TWBR=0x4f;
//TWBR=0xff;
//TWBR=0x2f;
TWSR = 0;
TWBR=40;
//TWBR=20;

TWDR = 0xFF;
TWCR = _BV(TWEN);
I2C_status=I2C_NO_ERROR;
sei();

}

uint8_t I2C_read_request(uint8_t node,uint8_t addr,uint8_t *buff,uint8_t size)
{

I2C_node=(node&0x0f)<<1;
I2C_addr=addr;
I2C_buff=buff;
I2C_reading=1;
I2C_status=I2C_BUSY;
I2C_len_R=size;
I2C_len_W=0;
I2C_size=I2C_len_W+3;
I2C_status=I2C_BUSY;
//RED_LIGHT_ON();

TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWSTA);

while(!I2C_status) continue;

return I2C_status-1;
}

uint8_t I2C_write_request(uint8_t node,uint8_t addr,uint8_t *buff,uint8_t size)
{
//PRINTF("%s\t%d\ti2c write request, node=%d, addr=%x, buff = %x, size = %d\n", node, addr, *buff, size);
I2C_node=(node&0x0f)<<1;
I2C_addr=addr;
I2C_buff=buff;
I2C_reading=0;
//I2C_CRC=0;
I2C_len_R=0;
I2C_len_W=size;
I2C_size=I2C_len_W+3;
I2C_status=I2C_BUSY;
//RED_LIGHT_ON();
TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWSTA);

while(!I2C_status) continue;

return I2C_status-1;
}



ISR (SIG_2WIRE_SERIAL)
{
static uint8_t I2C_CRC;
static uint8_t I2C_current;
uint8_t I2C_data;

//PRINTF("%s\t%d\tISR\t", __FILE__, __LINE__);

switch (TWSR & 0xf8)
	{
                                		//START
	case TWI_START:		// START has been transmitted  
	case TWI_REP_START:	// Repeated START has been transmitted
				//need send node address
/*
		switch(I2C_reading)
			{
			case 0://only write
			 	TWDR = I2C_node|I2C_MOD_WRITE;
				break;
			case 1://first write addr and requested len
			 	TWDR = I2C_node|I2C_MOD_WRITE;
				break;
			case 2://second read data
			 	TWDR = I2C_node|I2C_MOD_READ;
				break;
			}
*/				//same as above, but shorter
		if(I2C_reading==2)
			{
		 	TWDR = I2C_node|I2C_MOD_READ;
			}
		else
			{
		 	TWDR = I2C_node|I2C_MOD_WRITE;
			}
		I2C_data=TWDR;
	        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
		I2C_CRC=_crc_ibutton_update(0,I2C_data);
		I2C_current=0;
		break;

                                		//SEND

	case TWI_MTX_ADR_ACK:	// SLA+W has been tramsmitted and ACK received
	case TWI_MTX_DATA_ACK:	// Data byte has been tramsmitted and ACK received
				//send data in order:	node, addr, lenR, lenW, data ... data, CRC
		switch(I2C_current)
			{
			case 0:	//Write addr
				TWDR=I2C_addr;
				I2C_data=TWDR;
			        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
				I2C_CRC=_crc_ibutton_update(I2C_CRC,I2C_data);
				break;
			case 1:	//Write len to read
				TWDR=I2C_len_R;
				I2C_data=TWDR;
			        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
				I2C_CRC=_crc_ibutton_update(I2C_CRC,I2C_data);
				break;
			case 2:	//Write len to write
				TWDR=I2C_len_W;
				I2C_data=TWDR;
			        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
				I2C_CRC=_crc_ibutton_update(I2C_CRC,I2C_data);
				break;
			default:
				if (I2C_current < I2C_size)
					{
				 	TWDR = I2C_buff[I2C_current-3];
					I2C_data=TWDR;
	        			TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
					I2C_CRC=_crc_ibutton_update(I2C_CRC,I2C_data);
               				}
				else
					{//Write CRC
					if(I2C_current==I2C_size)
						{	//Write CRC
						TWDR=I2C_CRC;
		        			TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT);
               					}
               				else
						{
						if(I2C_reading)
							{//send REP_START for read
			                       	        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWSTA);
							I2C_reading=2;
							I2C_size=I2C_len_R;//read size
							}
						else
							{
							TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
							I2C_current-=5;	// 1(addr) + 1(len_R) + 1(len_W) + 1(CRC) + 1(STOP)
							I2C_status=I2C_NO_ERROR;
							}
						}
					}
				break;
			}
		I2C_current++;
		break;


	case TWI_MTX_ADR_NACK:	// SLA+W has been tramsmitted and NACK received 

		TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
		I2C_status=I2C_ERROR_NO_ANSWER;
		break;


	case TWI_MTX_DATA_NACK:	// Data byte has been tramsmitted and NACK received 
		TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
		I2C_status=I2C_ERROR_CANT_SEND_ALL_PACKET;
		break;


                                		//RECEIVE

	case TWI_MRX_ADR_ACK:	// SLA+R has been tramsmitted and ACK received
				//address sended and now we must receive
		if (I2C_current < I2C_size)
			{
		        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWEA);
			}
		else
			{//Error! we must receive node, lenR, data...data, CRC. I2C_size can't equal zero!
			TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
			I2C_status=I2C_ERROR_RECEIVE_SHORT_PACKET;
			}
		break;


	case TWI_MRX_DATA_ACK:	// Data byte has been received and ACK tramsmitted
				//All ok. we must receive node, lenR, data...data, CRC.
		I2C_data=TWDR;
		if(!I2C_current)
			{	//Receive lenR. check correct value

			if(I2C_len_R!=TWDR)
				{
				I2C_status=I2C_ERROR_INCORRECT_LEN_IN_RESPONSE;
					//Can't simply stop. need send NACK before
				TWCR=_BV(TWEN)|_BV(TWIE)|_BV(TWINT);
				}
			else
				{
			        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWEA);
				}
			}
		else
			{
			I2C_buff[I2C_current-1]=TWDR;

			if (I2C_current < I2C_size)
				{
			        TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWEA);
				}
		else                    // Send STOP after last byte
				{
				TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWIE);
				}
			}
		I2C_CRC=_crc_ibutton_update(I2C_CRC,I2C_data);
		I2C_current++;
		break;


	case TWI_MRX_DATA_NACK:	// Data byte has been received and NACK tramsmitted
				//last byte received. it must be CRC
		I2C_data=TWDR;
      		TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
		if(!I2C_status)
			{
			if(I2C_CRC==I2C_data)
				{	//All ok
				I2C_status=I2C_NO_ERROR;
				}
			else
				{
				I2C_status=I2C_ERROR_CRC_ERROR_IN_ANSWER;
				}
			}
		break;


	case TWI_ARB_LOST:	// Arbitration lost
				//Restart exchange
		TWCR = _BV(TWEN)|_BV(TWIE)|_BV(TWINT)|_BV(TWSTA);
		break;
	


	default:
		TWCR = _BV(TWEN)|_BV(TWINT)|_BV(TWSTO);
		I2C_status=I2C_ERROR_UNKNOWN;
		break;
	}
	//PRINTF("I2C_status = 0x%02x\n", I2C_status);
}


