#ifndef RELAY_H_
#define RELAY_H_

void relayInit(void);
void setRelay(uint8_t relay, uint8_t  value);
void setAllrelay(uint16_t value);
uint8_t getRelay(uint8_t relay);
uint16_t getAllrelay();


#endif //RELAY_H_