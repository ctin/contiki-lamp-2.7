/*
 * Copyright (c) 2006, Technical University of Munich
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * @(#)$$
 */

#include <avr/pgmspace.h>
#include <avr/fuse.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include <stdlib.h>     /* strtol */
#include <string.h>

#include "lib/mmem.h"
#include "loader/symbols-def.h"
#include "loader/symtab.h"

#define ANNOUNCE_BOOT 0    //adds about 600 bytes to program size
#define DEBUG 1
#if DEBUG
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

#if RF230BB           //radio driver using contiki core mac
#include "radio/rf230bb/rf230bb.h"
#include "net/mac/frame802154.h"
#include "net/mac/framer-802154.h"
#include "net/sicslowpan.h"
#else                 //radio driver using Atmel/Cisco 802.15.4'ish MAC
#include <stdbool.h>
#include <stdlib.h>
#include "mac.h"
#include "sicslowmac.h"
#include "sicslowpan.h"
#include "ieee-15-4-manager.h"
#endif //RF230BB

#include "contiki.h"
#include "contiki-net.h"
#include "contiki-lib.h"

#include "dev/rs232.h"
#include "dev/serial-line.h"
#include "dev/slip.h"

#include "sicslowmac.h"
#include "../../ctinMacro.h"

#ifdef THL
#include "sensor_light.h"
#include "sensor_humidity_temperature.h"
#endif //THL

#ifdef TELEKONT
#include "telekont.h"
#endif //TELEKONT

#ifdef SLEEP_MODE
#include "sleep.h"
#endif //SLEEP_MODE

#ifdef RELAY
#include "relay.h"
#endif //RELAY

#ifdef WATER
#include "water.h"
#endif //WATER

#ifdef DIMMER
#include "dimmer.h"
#endif //DIMMER

#ifdef PIR
#include "sensor_pir.h"
#endif //PIR

#ifdef AIR
#include "sensor_air.h"
#endif //AIR

#ifdef TWI_MODULE
#include "telekont_variables.h"
#include "i2c_master.h"
#include "access.h"
#endif //TWI_MODULE

FUSES =
	{
		.low = (FUSE_CKSEL0 & FUSE_CKSEL2 & FUSE_CKSEL3 & FUSE_SUT0), // 0xe2,
		.high = (FUSE_BOOTSZ0 /*& FUSE_BOOTSZ1*/ & FUSE_SPIEN & FUSE_JTAGEN), //0x9D,
		.extended = 0xff,
	};
	
#if RF230BB
//PROCINIT(&etimer_process, &tcpip_process );
#else
PROCINIT(&etimer_process, &mac_process, &tcpip_process );
#endif
/* Put default MAC address in EEPROM */
#ifdef ALEX
#warning "ALEX, ip 0x02, 0x11, 0x22, 0xff, 0xfe, 0x33, 0x44, 0x5?"
uint8_t mac_address[8] EEMEM = {0x02, 0x11, 0x22, 0xff, 0xfe, 0x33, 0x44, 0x50 + ALEX};
#endif //ALEX
#ifdef JOKER
#warning "JOKER, ip 0x02, 0x11, 0x22, 0xff, 0xfe, 0x33, 0x44, 0x6?"
uint8_t mac_address[8] EEMEM = {0x02, 0x11, 0x22, 0xff, 0xfe, 0x33, 0x44, 0x60 + JOKER};
#endif //JOKER
#ifdef TELEKONT
#warning "TELEKONT, ip 20 + TELEKONT VALUE"
uint8_t mac_address[8] EEMEM = {0x02, 0x11, 0x22, 0xff, 0xfe, 0x33, 0x44, 0x20 + TELEKONT};
#endif //TELEKONT

#define RX_BUFFER_SIZE 40

static char rx_buffer[RX_BUFFER_SIZE];
static unsigned char rx_index;
static unsigned char dataForInput;
static unsigned char dataHandled;
static int inputHandler(unsigned char data) {
	dataForInput = data;
	dataHandled = 1;
	return 0;
}
void
init_lowlevel(void)
{

  /* Second rs232 port for debugging */
  rs232_init(RS232_PORT_1, USART_BAUD_38400,
             USART_PARITY_NONE | USART_STOP_BITS_1 | USART_DATA_BITS_8);			 

  /* Redirect stdout to second port */
  rs232_redirect_stdout(RS232_PORT_1);
  
  /* Clock */
  clock_init();
 
 /* rtimers needed for radio cycling */
  rtimer_init();

 /* Initialize process subsystem */
  process_init();
 /* etimers must be started before ctimer_init */
  process_start(&etimer_process, NULL);
  
#if RF230BB

  ctimer_init();
  /* Start radio and radio receive process */
  NETSTACK_RADIO.init();

  /* Set addresses BEFORE starting tcpip process */

  rimeaddr_t addr;
  memset(&addr, 0, sizeof(rimeaddr_t));
  eeprom_read_block ((void *)&addr.u8,  &mac_address, 8);
 
#if UIP_CONF_IPV6
  memcpy(&uip_lladdr.addr, &addr.u8, 8);
#endif  
  rf230_set_pan_addr(IEEE802154_PANID, 0, (uint8_t *)&addr.u8);
#ifdef CHANNEL_802_15_4
  rf230_set_channel(CHANNEL_802_15_4);
#else
  rf230_set_channel(26);
#endif

  rimeaddr_set_node_addr(&addr); 

  PRINTF("MAC address %x:%x:%x:%x:%x:%x:%x:%x\n",addr.u8[0],addr.u8[1],addr.u8[2],addr.u8[3],addr.u8[4],addr.u8[5],addr.u8[6],addr.u8[7]);

  /* Initialize stack protocols */
  queuebuf_init();
  NETSTACK_RDC.init();
  NETSTACK_MAC.init();
  NETSTACK_NETWORK.init();

#if ANNOUNCE_BOOT
  printf_P(PSTR("%s %s, channel %u"),NETSTACK_MAC.name, NETSTACK_RDC.name,rf230_get_channel());
  if (NETSTACK_RDC.channel_check_interval) {//function pointer is zero for sicslowmac
    unsigned short tmp;
    tmp=CLOCK_SECOND / (NETSTACK_RDC.channel_check_interval == 0 ? 1:\
                                   NETSTACK_RDC.channel_check_interval());
    if (tmp<65535) printf_P(PSTR(", check rate %u Hz"),tmp);
  }
  printf_P(PSTR("\n"));
#endif

#if UIP_CONF_ROUTER
#if ANNOUNCE_BOOT
  printf_P(PSTR("Routing Enabled\n"));
#endif
 // rime_init(rime_udp_init(NULL));
 // uip_router_register(&rimeroute);
#endif

  process_start(&tcpip_process, NULL);

#else
/* mac process must be started before tcpip process! */
  process_start(&mac_process, NULL);
  process_start(&tcpip_process, NULL);
#endif /*RF230BB*/
rs232_set_input(RS232_PORT_1, inputHandler);
  {
	int i;
  	for(i = 0;  i < RX_BUFFER_SIZE;  i++)
		rx_buffer[i] = '\0';
	}
}

#define PRINTF_P printf_P
static void
ipaddr_add(const uip_ipaddr_t *addr)
{
  uint16_t a;
  int8_t i, f;
  for(i = 0, f = 0; i < sizeof(uip_ipaddr_t); i += 2) {
    a = (addr->u8[i] << 8) + addr->u8[i + 1];
    if(a == 0 && f >= 0) {
      if(f++ == 0) PRINTF_P(PSTR("::"));
    } else {
      if(f > 0) {
        f = -1;
      } else if(i > 0) {
	    PRINTF_P(PSTR(":"));
      }
	  PRINTF_P(PSTR("%x"),a);
    }
  }
} 

void printAllAdresses()
{
	unsigned char i,j;
	uip_ds6_nbr_t *nbr;
	PRINTF_P(PSTR("\n\rAddresses [%u max]\n\r"),UIP_DS6_ADDR_NB);
	for (i=0;i<UIP_DS6_ADDR_NB;i++) {
		if (uip_ds6_if.addr_list[i].isused) {	  
			ipaddr_add(&uip_ds6_if.addr_list[i].ipaddr);
			PRINTF_P(PSTR("\n\r"));
		}
	}
	PRINTF_P(PSTR("\n\rNeighbors [%u max]\n\r"),NBR_TABLE_MAX_NEIGHBORS);

	for(nbr = nbr_table_head(ds6_neighbors);
		nbr != NULL;
		nbr = nbr_table_next(ds6_neighbors, nbr)) {
	  ipaddr_add(&nbr->ipaddr);
	  PRINTF_P(PSTR("\n\r"));
	  j=0;
	}
	if (j) PRINTF_P(PSTR("  <none>"));
	PRINTF_P(PSTR("\n\rRoutes [%u max]\n\r"),UIP_DS6_ROUTE_NB);
	uip_ds6_route_t *route;
	for(route = uip_ds6_route_head();
	route != NULL;
	route = uip_ds6_route_next(route)) {
		ipaddr_add(&route->ipaddr);
		PRINTF_P(PSTR("/%u (via "), route->length);
		ipaddr_add(uip_ds6_route_nexthop(route));
		if(route->state.lifetime < 600) {
			PRINTF_P(PSTR(") %lus\n\r"), route->state.lifetime);
		 } else {
			PRINTF_P(PSTR(")\n\r"));
		}
		j=0;
	}
	if (j) PRINTF_P(PSTR("  <none>"));
	PRINTF_P(PSTR("\n\r---------\n\r"));
}

process_event_t sendPacketEvent;
#include "net/rpl/rpl-private.h"

int input()
{
	unsigned char data = dataForInput;
	if(dataHandled)
		dataHandled = 0;
	else
		return 0;
#ifdef DIMMER
	if(data == 'o')
	{
		collectDimmerCommad(rx_buffer, RX_BUFFER_SIZE);
		rx_index = 0;
	}
	else if((data >= '0' && data <= '9') || data == 'p')
	{
		rx_buffer[rx_index++]=data;
		if(rx_index >= RX_BUFFER_SIZE)
			rx_index = 0;
	}
	else
#endif //DIMMER
#ifdef TWI_MODULE
	if(data == 'o')
	{	
		uint8_t i;
		if(rx_buffer[0] == 'S')
		{
			uint8_t ret;
			char * pEnd = rx_buffer + 4;
			uint8_t module = rx_buffer[1] - '0';
			uint16_t value = strtol(rx_buffer + 2, &pEnd, 16);
			printf("will set value %x to module %d... ", value, module);
			ret=SetDiskretChannels(module,(value >> 8),value,0,0);
			printf("%s\n", ret ? "fail" : "success");
		}
		for(i = 0;  i < RX_BUFFER_SIZE;  i++)
			rx_buffer[i] = '\0';		
		rx_index = 0;
	}
	else if((data >= '0' && data <= '9') || (data >= 'a' && data <= 'f') || (data >= 'A' && data <= 'F') || data == 'S')
	{
		rx_buffer[rx_index++]=data;
		if(rx_index >= RX_BUFFER_SIZE)
			rx_index = 0;
	}
	else
#endif //TWI_MODULE

#ifdef RELAY
	if(data == 'o')
	{	
		uint8_t i;
		if(rx_buffer[0] == 'S')
		{
			uint8_t relay = rx_buffer[1] - '0';
			uint16_t value = atoi(rx_buffer + 2);
			printf("will set value %x to relay %d... ", value, relay);
			setRelay(relay,value);
		}
		for(i = 0;  i < RX_BUFFER_SIZE;  i++)
			rx_buffer[i] = '\0';		
		rx_index = 0;
	}
	else if((data >= '0' && data <= '9') || (data >= 'a' && data <= 'f') || (data >= 'A' && data <= 'F') || data == 'S')
	{
		rx_buffer[rx_index++]=data;
		if(rx_index >= RX_BUFFER_SIZE)
			rx_index = 0;
	}
	else
#endif //RELAY

	switch(data)
	{
	case 'h':
		printf("press 'p' to send IPv6 UDP packet to server\n");
		printf("press 'n' to print only neighbours\n");
		printf("press 's' to send DIS to all\n");
		printf("press 'r' to reset chip\n");
		printf("press 'L' to test LED1\n");
		printf("press 'P' to get all GPIO states\n");
#ifdef THL
		printf("press 'l' to print light sensor\n");
		printf("press 't' to print humidity&temperature sensor\n");
#endif //THL
#ifdef SLEEP_MODE
		printf("press 'D' to put to sleep power down\n");
#endif //SLEEP_MODE
#ifdef RELAY
		printf("press 'S' Relay Num,Value,'o'");
#endif //RELAY
#ifdef WATER
		printf("press 'w' to get water sensor state\n");
		printf("press 'W' to toggle water sensor state\n");
#endif //WATER
#ifdef DIMMER
		printf("press 'p***o' to set dimmer percentage\n");
		printf("press 'd' to print dimmer timings\n");
#endif //DIMMER
#ifdef PIR
		printf("press 'i' to read PIR sensor\n");
#endif //PIR
#ifdef AIR
		printf("preass 'a' to get air sensor state\n");
#endif //AIR
#ifdef TWI_MODULE
		printf("press 'T' to print i2c modules telekontConsist\n");
		printf("press 'g' to get all discret channes states\n");
		printf("press 'S' Module Num,Value 16bit,'o' (S1f2o) to toggle first channel in firsr valid module\n");
		printf("press 'I' to check consist\n");
#endif //TWI_MODULE
		break;
	case 'L':
		printf("LED 1 toggled\n"); toggle_pin(PORT(LED1_PORT), LED1_PIN); break;
		break;
	case 'P':
		printf("GPIO1 DDR  state: 0x%02x\n", DDR(GPIO1_PORT));
		printf("GPIO1 PORT state: 0x%02x\n", PIN(GPIO1_PORT));
		printf("\n");
		printf("GPIO2 DDR  state: 0x%02x\n", DDR(GPIO2_PORT));
		printf("GPIO2 PORT state: 0x%02x\n", PIN(GPIO2_PORT));
		printf("\n");
		printf("GPIO3 DDR  state: 0x%02x\n", DDR(GPIO3_PORT));
		printf("GPIO3 PORT state: 0x%02x\n", PIN(GPIO3_PORT));
		printf("\n");
		printf("GPIO4 DDR  state: 0x%02x\n", DDR(GPIO4_PORT));
		printf("GPIO4 PORT state: 0x%02x\n", PIN(GPIO4_PORT));
		printf("\n");
		printf("GPIO5 DDR  state: 0x%02x\n", DDR(GPIO5_PORT));
		printf("GPIO5 PORT state: 0x%02x\n", PIN(GPIO5_PORT));
		printf("\n");
		printf("GPIO6 DDR  state: 0x%02x\n", DDR(GPIO6_PORT));
		printf("GPIO6 PORT state: 0x%02x\n", PIN(GPIO6_PORT));
		printf("\n");
		printf("GPIO7 DDR  state: 0x%02x\n", DDR(GPIO7_PORT));
		printf("GPIO7 PORT state: 0x%02x\n", PIN(GPIO7_PORT));
		printf("\n");
	case 'n':			
		printAllAdresses();
		break;
	case 'p':
		printf("posting event to sending packet.\t");
		printf("Result: %x\n", process_post(PROCESS_BROADCAST, sendPacketEvent, PROCESS_ERR_OK) == PROCESS_ERR_OK);
		break;
	case 's':
		printf("sending dis\n");
		dis_output(NULL);
		break;
	case 'r':
		printf("going to reset\n");
		{((void(*)(void))0)();}
		break;
#ifdef THL
	case 'l':
		printf("light sensor: %d\n", refreshLux(1));
		break;
	case 't':
		SHTgetData();
		printf("humidity: %d, temperature: %d\n", g_humidity, g_temperature);
		break;
#endif //THL
#ifdef SLEEP_MODE
	case 'D': 
		{
		printf("entering sleep mode\n");
		setCanSleep(CAN_SLEEP_SUCCESS);
		}
		break;
#endif 
#ifdef WATER
	case 'w': printf("water sensor state = %d\n", getWaterSensorData()); break;
	case 'W': 
		setWaterSensorState(!getWaterSensorState());
		printf("water sensor state set to %d\n", getWaterSensorState());
		break;
#endif //WATER
#ifdef PIR
	case 'i': 
		printf("PIR sensor state: 0x%02x\n", getPIRsensorState()); 
		break;
#endif //PIR
#ifdef AIR
	case 'a': 
		{
			uint16_t state1 = readAirSensor(0);
			uint16_t state2 = readAirSensor(1);
			printf("Air sensor state: channel1 = %d, 0x%x\t channel2 = %d, 0x%x\n", state1, state1, state2, state2);
		}
		break;
#endif //AIR
#ifdef DIMMER
	case 'd': 
		printDimmerData(); 
		break;
#endif //DIMMER
#ifdef TWI_MODULE
	case 'T': 
		{
			uint8_t slot, i;
			printf("i2c telekontConsist\n");
			for(i = 0;  i < 3;  i++) 
			{	
				printf("\t");
				for(slot = 0;  slot < MAX_SLOT;  slot++)
				{
					printf("%02x ", telekontConsist[slot][i]);
				}
				printf("\n");
			}
		}
		break;
	case 'g':
		{
			uint8_t slot;
			uint8_t buffer[2];
			uint8_t ret;
			printf("i2c telekontConsist\t");
			sei();
			for(slot = 0;  slot < MAX_SLOT;  slot++)
			{
				if(telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST || telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG)
					continue;
				printf("slot: %d...\t", slot);
				ret=GetDiskretChannels(slot,buffer,buffer+1,0,0);
				if(!ret)
					printf("values: %x %x\n", buffer[0], buffer[1]);
			}
			printf("\n");
		}
	break;
	case 'I':
		printf("checking... ");
		consist_check();
		printf("ok!\n");
		break;
#endif //TWI_MODULE
	case ' ':
		printf(" ");
	break;
	case '\n':
	case '\t':
	case '\r':
		//printf(">\n");
	break;
	default:
		printf("press h to menu\n");
	break;
	}
	printf(">\n");
	return 0;
}  


int
main(void)
{
  //calibrate_rc_osc_32k(); //CO: Had to comment this out

  /* Initialize hardware */
	init_lowlevel();
	sendPacketEvent = process_alloc_event();
#ifdef LED1_PORT
	sbi(DDR(LED1_PORT), LED1_PIN);
	sbi(PORT(LED1_PORT), LED1_PIN);
#endif //LED1_PORT
#ifdef LED2_PORT
	sbi(DDR(LED2_PORT), LED2_PIN);
	sbi(PORT(LED2_PORT), LED2_PIN);
#endif //LED2_PORT
#ifdef LED3_PORT
	sbi(DDR(LED3_PORT), LED3_PIN);
	sbi(PORT(LED3_PORT), LED3_PIN);
#endif //LED3_PORT
#ifdef BUTTON1_PORT
	cbi(DDR(BUTTON1_PORT), BUTTON1_PIN);
	sbi(PORT(BUTTON1_PORT), BUTTON1_PIN);
#endif //BUTTON1_PORT
#ifdef BUTTON2_PORT
	cbi(DDR(BUTTON2_PORT), BUTTON2_PIN);
	sbi(PORT(BUTTON2_PORT), BUTTON2_PIN);
#endif //BUTTON2_PORT

#ifdef TELEKONT
	initModule();
	PRINTF("%s\t%d\tTELEKONT inited\n", __FILE__, __LINE__);
#endif //TELEKONT


#ifdef RELAY
	relayInit();
	PRINTF("%s\t%d\tRelay inited\n", __FILE__, __LINE__);
#endif //RELAY

  /* Register initial processes */
#ifdef THL
	SHTgetData();
	refreshLux(1);
	PRINTF("%s\t%d\tTHL inited\n", __FILE__, __LINE__);
#endif //THL
#ifdef SLEEP_MODE
	initSleep();
	PRINTF("%s\t%d\tSLEEP_MODE inited\n", __FILE__, __LINE__);
#endif //SLEEP_MODE
#ifdef WATER
	waterSensorInit();
	PRINTF("%s\t%d\tWATER inited\n", __FILE__, __LINE__);
#endif //WATER
#ifdef DIMMER
	initDimmer();
	PRINTF("%s\t%d\tDIMMER inited\n", __FILE__, __LINE__);
#endif //DIMMER
#ifdef PIR
	initPIRsensor();
	PRINTF("%s\t%d\tPIR inited\n", __FILE__, __LINE__);
#endif //PIR
#ifdef AIR
	initAirSensor();
	PRINTF("%s\t%d\tAir inited\n", __FILE__, __LINE__);
#endif //AIR
#ifdef TWI_MODULE
	I2C_init();
	consist_init();
#endif //TWI_MODULE
  /* Autostart processes */

	printf_P(PSTR("\n********BOOTING CONTIKI*********\n"));

	printf_P(PSTR("System online.\n"));
  
	autostart_start(autostart_processes);
	printf("%s\t%d\tall inited\n", __FILE__, __LINE__);
  /* Main scheduler loop */
  while(1) {
    process_run();
	input();
  }

  return 0;
}
