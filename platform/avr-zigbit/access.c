#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <string.h>
#include "../../ctinMacro.h"

#include "telekont_variables.h"
#include "i2c_master.h"
#include "access.h"

#define DEBUG 0
#if DEBUG
#include <avr/pgmspace.h>
#define PRINTF(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#define PRINTSHORT(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
#else
#define PRINTF(...)
#define PRINTSHORT(...)
#endif

uint8_t telekontConsist[MAX_SLOT][CONSIST_SAVE_SIZE];
static uint16_t consist_error = 0;

void consist_init(void)
{
	uint8_t i;
	for (i=0;i<MAX_SLOT;i++)
	{
		if(I2C_read_request(i,0,telekontConsist[i],CONSIST_SAVE_SIZE))
		{
			memset(telekontConsist[i],0xff,CONSIST_SAVE_SIZE);
		}
	}
	consist_error=0;
}


void consist_check(void)
{
	uint8_t check_slot;
	uint8_t tmp_buff[CONSIST_CHECK_SIZE];
	PRINTF("%s\t%d\tconsist check", __FILE__, __LINE__);
	for(check_slot = 0;  check_slot < MAX_SLOT;  check_slot++) 
	{
		//if(telekontConsist[check_slot][ADDR_TYPE]==0xff)
		//		return;	//дык его и не было. даже не будем искать!
		if(I2C_read_request(check_slot,0,tmp_buff,CONSIST_CHECK_SIZE))
		{
			consist_error|=_BV16(check_slot);
		}
		else
		{
			if(memcmp(telekontConsist[check_slot],tmp_buff,CONSIST_CHECK_SIZE))
				consist_error|=_BV16(check_slot);
			else
				consist_error&=~_BV16(check_slot);
		}
	}
	PRINTF("\tchecked!\n");
}





uint8_t GetDiskretChannels(
	uint8_t slot,
	uint8_t *Hi,
	uint8_t *Lo,
	uint8_t *Hi_check,
	uint8_t *Lo_check
)
{
	uint8_t tmp_buff[4];
	PRINTF("%s\t%d\tGetDiskretChannels\n", __FILE__, __LINE__);
if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST )) return ERR_NO_SUCH_MODULE; 
if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG )) return ERR_SLOT_IS_ANALOG; 

if(!telekontConsist[slot][ADDR_DATA])
	return ERR_BROKEN_MODULE;

PRINTF("%s\t%d\tbefore I2C_read_request\n", __FILE__, __LINE__);
if(I2C_read_request(slot,telekontConsist[slot][ADDR_DATA],tmp_buff,4))
	{
		PRINTF("%s\t%d\tfailed\n", __FILE__, __LINE__);

	return ERR_BROKEN_MODULE;
	}

*Lo=tmp_buff[0];

if (telekontConsist[slot][ADDR_CHANNELS]<9) //8 channels only
	*Hi=0;
else
	*Hi=tmp_buff[1];

if(Lo_check) *Lo_check=tmp_buff[2];
if(Hi_check) *Hi_check=tmp_buff[3];
PRINTF("%s\t%d\tready\n", __FILE__, __LINE__);

return 0;
}





uint8_t SetDiskretChannels(
	uint8_t slot,
	uint8_t Hi, 
	uint8_t Lo,
	uint8_t *Hi_check,
	uint8_t *Lo_check
)
{               
uint8_t tmp_buff[2];

if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST )) return ERR_NO_SUCH_MODULE;
if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG )) return ERR_SLOT_IS_ANALOG;
if (!(telekontConsist[slot][ADDR_TYPE]&MODULE_FOR_OUTPUT)) return ERR_SLOT_FOR_READ;    


if(!telekontConsist[slot][ADDR_DATA])
	return ERR_BROKEN_MODULE;

tmp_buff[0]=Lo;
tmp_buff[1]=Hi;

if(I2C_write_request(slot,telekontConsist[slot][ADDR_DATA],tmp_buff,2))
	{
	return ERR_BROKEN_MODULE+1;
	}
/*
#if DEBUG
usart1_puts_P(PSTR("\r\nsend: "));
printhex(tmp_buff[0]);
printhex(tmp_buff[1]);
#endif
*/


if(( (int) Hi_check || (int) Lo_check ))
	{
	delay_between_requests();
	if(I2C_read_request(slot,telekontConsist[slot][ADDR_DATA]+2,tmp_buff,2))
		{
		return ERR_BROKEN_MODULE+2;
		}
/*
#if DEBUG
usart1_puts_P(PSTR("\r\nreaded: "));
printhex(tmp_buff[0]);
printhex(tmp_buff[1]);
printpointer(Hi_check);
printpointer(Lo_check);
#endif
*/
	if(Lo_check)
		*Lo_check=tmp_buff[0];
	if(Hi_check)
		*Hi_check=tmp_buff[1];
	}                        

return 0;
}




//-------------------------------------------   
uint8_t SetOneDiskretChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t Value,
	uint8_t *check
	) 
{               
uint8_t tmp_buff[2];
uint8_t ret;
if ( ( telekontConsist[slot][ADDR_TYPE] & MODULE_NOT_EXIST  )) return ERR_NO_SUCH_MODULE; 
if ( ( telekontConsist[slot][ADDR_TYPE] & MODULE_IS_ANALOG  )) return ERR_SLOT_IS_ANALOG;
if (!( telekontConsist[slot][ADDR_TYPE] & MODULE_FOR_OUTPUT )) return ERR_SLOT_FOR_READ; 

if (Channel>=telekontConsist[slot][ADDR_CHANNELS]) return ERR_UNREAL_CHANNEL; // Error - unreal channel

if(!telekontConsist[slot][ADDR_DATA])
	return ERR_BROKEN_MODULE;
ret=I2C_read_request(slot,telekontConsist[slot][ADDR_DATA],tmp_buff,2);

/*
#if DEBUG
usart1_puts_P(PSTR("\r\nreaded: "));
printhex(tmp_buff[0]);
printhex(tmp_buff[1]);
#endif
*/
if(ret)
	{
//	return ERR_BROKEN_MODULE;
	return ret+0x10;
	}

if(Value)
	tmp_buff[Channel>>3]|=1<<(Channel%8);
else
	tmp_buff[Channel>>3]&=~(1<<(Channel%8));

delay_between_requests();

/*
#if DEBUG
usart1_puts_P(PSTR("\r\nsend: "));
printhex(tmp_buff[0]);
printhex(tmp_buff[1]);
#endif
*/

ret=I2C_write_request(slot,telekontConsist[slot][ADDR_DATA],tmp_buff,2);
if(ret)
	{
//	return ERR_BROKEN_MODULE;
	return ret+0x20;
	}

if(check)
	{
	if(telekontConsist[slot][ADDR_DIAG])
		{
		delay_between_requests();
		ret=I2C_read_request(slot,telekontConsist[slot][ADDR_DIAG]+Channel,check,1);
		if(ret)
			{
//			return ERR_BROKEN_MODULE;
			return ret+0x30;

			}
		}
	else
		*check=0;
	}

return 0;
}






uint8_t GetAnalogChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t *Hi,
	uint8_t *Lo,
	uint8_t *check 
)
{
uint8_t tmp_buff[3];

if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST )) return ERR_NO_SUCH_MODULE;
if (!(telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG )) return ERR_SLOT_IS_DISCRET;
if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_FOR_OUTPUT)) return ERR_SLOT_FOR_WRITE;


if (Channel>=telekontConsist[slot][ADDR_CHANNELS]) return ERR_UNREAL_CHANNEL;


if(!telekontConsist[slot][ADDR_DATA])
	return ERR_BROKEN_MODULE;


if(I2C_read_request(slot,telekontConsist[slot][ADDR_DATA]+Channel*CHANNEL_BYTES,tmp_buff,3))
	{
	return ERR_BROKEN_MODULE;
	}

*Lo=tmp_buff[0];
*Hi=tmp_buff[1];

if(check)
	*check=tmp_buff[2];

return 0;
}



uint8_t SetAnalogChannel(
	uint8_t slot,
	uint8_t Channel,
	uint8_t Hi,
	uint8_t Lo,
	uint8_t *check
)
{
uint8_t tmp_buff[2];

if ( (telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST )) return ERR_NO_SUCH_MODULE;
if (!(telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG )) return ERR_SLOT_IS_DISCRET;
if (!(telekontConsist[slot][ADDR_TYPE]&MODULE_FOR_OUTPUT)) return ERR_SLOT_FOR_READ;

if (Channel>=telekontConsist[slot][ADDR_CHANNELS]) return ERR_UNREAL_CHANNEL;

if(!telekontConsist[slot][ADDR_DATA])
	return ERR_BROKEN_MODULE;


tmp_buff[0]=Lo;
tmp_buff[1]=Hi;

if(I2C_write_request(slot,telekontConsist[slot][ADDR_DATA]+(Channel<<1),tmp_buff,2))
	{
	return ERR_BROKEN_MODULE;
	}


if(check)
	{
	if(telekontConsist[slot][ADDR_DIAG])
		{
		if(I2C_read_request(slot,telekontConsist[slot][ADDR_DIAG]+Channel,check,1))
			{
			return ERR_BROKEN_MODULE;
			}
		}
	else
		*check=0;
	}


return 0;

}

