#ifndef CTINMACRO_H
#define CTINMACRO_H

#include <avr/sfr_defs.h>


#define sbi(PORT, PIN)	(PORT |= _BV(PIN))
#define cbi(PORT, PIN)	(PORT &=~(_BV(PIN)))

#define toggle_pin(PORT, PIN) if(bit_is_set((PORT), (PIN))) \
cbi((PORT), (PIN)); \
else \
sbi((PORT), (PIN));
#ifndef CAT
#define CAT(x, y)      x##y
#endif //CAT
#ifndef PIN
#define PIN(x)         CAT(PIN,  x)
#endif //PIN
#ifndef DDR
#define DDR(x)         CAT(DDR,  x)
#endif //DDR
#ifndef PORT
#define PORT(x)        CAT(PORT, x)
#endif //PORT

#define GPIO0_PORT	B
#define GPIO1_PORT	B
#define GPIO2_PORT	B
#define GPIO3_PORT	G
#define GPIO4_PORT	G
#define GPIO5_PORT	G
#define GPIO6_PORT	D
#define GPIO7_PORT	D
#define GPIO8_PORT	E
#define GPIO9_PORT	G

#define GPIO0_PIN	5
#define GPIO1_PIN	6
#define GPIO2_PIN	7
#define GPIO3_PIN	0
#define GPIO4_PIN	1  
#define GPIO5_PIN	2
#define GPIO6_PIN	6
#define GPIO7_PIN	7
#define GPIO8_PIN	3
#define GPIO9_PIN	5

#ifdef ALEX

#define LED1_PORT	D//(GPIO7)
#define LED2_PORT	D//(GPIO6)
#define LED3_PORT	G//(GPIO5)
#define LED1_PIN	7//(GPIO7)
#define LED2_PIN	6//(GPIO6)
#define LED3_PIN	2//(GPIO5)

#define BUTTON1_PORT	B//(GPIO0)
#define BUTTON2_PORT	B//(GPIO1)
#define BUTTON1_PIN	5//(GPIO0)
#define BUTTON2_PIN	6//(GPIO1)
#endif //ALEX

#ifdef JOKER
#define LED1_PORT	G//(GPIO3)
#define LED2_PORT	G//(GPIO4)
#define LED3_PORT	G//(GPIO5)
#define LED1_PIN	0//(GPIO3)
#define LED2_PIN	1//(GPIO4)
#define LED3_PIN	2//(GPIO5)

#define BUTTON1_PORT	D//(GPIO6)
#define BUTTON2_PORT	D//(GPIO7)
#define BUTTON1_PIN	6//(GPIO6)
#define BUTTON2_PIN	7//(GPIO7)
#endif //JOKER

#ifdef TELEKONT
#define LED2_ERR_PORT	G//(GPIO9)
#define LED2_RUN_PORT	E//(GPIO8)
#define LED2_ERR_PIN	5//(GPIO9)
#define LED2_RUN_PIN	3//(GPIO8)
#if RELAY == 1
#define LED1_PORT	G//(GPIO3)
#define LED1_PIN	0//(GPIO3)
#define LED2_PORT	G//(GPIO4)
#define LED2_PIN	1//(GPIO4)
#define RELAY0_PORT	B//(GPIO2)
#define RELAY0_PIN	7//(GPIO2)
#endif //RELAY == 1
#if RELAY == 4
#define LED1_PORT	G//(GPIO5)
#define LED1_PIN	2//(GPIO5) //3 и 4
#define RELAY0_PORT	B//(GPIO1) //2
#define RELAY0_PIN	6//(GPIO1)
#define RELAY1_PORT	B//(GPIO2)
#define RELAY2_PORT	G//(GPIO3)
#define RELAY3_PORT	G//(GPIO4)
#define RELAY1_PIN	7//(GPIO2)
#define RELAY2_PIN	0//(GPIO3)
#define RELAY3_PIN	1//(GPIO4)
#endif //RELAY == 4
#ifdef WATER
#define WATER_PORT	B//(GPIO2)
#define WATER_PIN	7//(GPIO2)
#define WATER_ADC	1//второй
#endif //WATER
#ifdef DIMMER
#define BUTTON1_PORT	D //(GPIO6)
#define BUTTON1_PIN		6 //(GPIO6)
#define BUTTON2_PORT	D //(GPIO7)
#define BUTTON2_PIN		7 //(GPIO7)
#define LED1_PORT	G//(GPIO3)
#define LED1_PIN	0//(GPIO3)
#define LED2_PORT	G//(GPIO4)
#define LED2_PIN	1//(GPIO4)
#define DIMMER_INPUT_PORT	B //(GPIO1)
#define DIMMER_OUTPUT_PORT	B //(GPIO2)
#define DIMMER_INPUT_PIN	6 // GPIO1
#define DIMMER_OUTPUT_PIN	7 // GPIO2
#endif //DIMMER
#ifdef PIR
#define BUTTON1_PORT	D //(GPIO6)
#define BUTTON1_PIN		6 //(GPIO6)
#define BUTTON2_PORT	D //(GPIO7)
#define BUTTON2_PIN		7 //(GPIO7)
#define LED1_PORT	G//(GPIO3)
#define LED1_PIN	0//(GPIO3)
#define LED2_PORT	G//(GPIO4)
#define LED2_PIN	1//(GPIO4)
#define PIR_PORT	B//(GPIO1)
#define PIR_PIN	6//(GPIO1)
#endif //PIR
#ifdef TWI_MODULE
#define BUTTON1_PORT	D //(GPIO6)
#define BUTTON1_PIN		6 //(GPIO6)
#define BUTTON2_PORT	D //(GPIO7)
#define BUTTON2_PIN		7 //(GPIO7)
#define LED1_PORT	G//(GPIO3)
#define LED1_PIN	0//(GPIO3)
#define LED2_PORT	G//(GPIO4)
#define LED2_PIN	1//(GPIO4)
#endif //TWI_MODULE
#ifdef AIR
#define AIR_ADC_CHANNEL_1	(1)
#define AIR_ADC_CHANNEL_2	(2)
#define AIR_FACTOR	(2) 	//для пересчета показаний АЦП в реальные PPM CO2. В диапазоне от 400 до 1200 показывало верно
#define AIR_ADC_VREF_TYPE 0x40
#endif //AIR
#endif //TELEKONT

#endif //CTINMACRO_H