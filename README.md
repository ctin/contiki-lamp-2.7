Прошивка беспроводных IPv6 (6LoWPAN) модулей на базе Contiki-OS.

Основные изменения - генерация и разбор JSON для UDP пакета в файле ./examples/ipv6/rpl-udp/udp-client.c, портирование под конкретные модули в папке /platform/avr-zigbit. Много незначительных изменений во всем проекте для проверки системных функций на устройствах. 

Сборка осуществляется из директории ./examples/ipv6/rpl-udp Среда разработки: компиллятор avr-gcc. 

ПО предназначено для чипов различных производителей, в данном проекте используются модули Atmel zigbit.

Проверка работоспособности осуществляется любой программой, обрабатывающей UDP IPv6 пакеты: tcpdump (Linux), wireshark (Windows) или любым UDP IPv6 сервером.

The Contiki Operating System
============================

[![Build Status](https://secure.travis-ci.org/contiki-os/contiki.png)](http://travis-ci.org/contiki-os/contiki)

Contiki is an open source operating system that runs on tiny low-power
microcontrollers and makes it possible to develop applications that
make efficient use of the hardware while providing standardized
low-power wireless communication for a range of hardware platforms.

Contiki is used in numerous commercial and non-commercial systems,
such as city sound monitoring, street lights, networked electrical
power meters, industrial monitoring, radiation monitoring,
construction site monitoring, alarm systems, remote house monitoring,
and so on.

For more information, see the Contiki website:

[http://contiki-os.org](http://contiki-os.org)