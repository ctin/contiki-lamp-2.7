/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */ 
  
#include "contiki.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "net/uip.h"
#include "net/uip-ds6.h"
#include "net/uip-udp-packet.h"
#include "sys/ctimer.h"
#ifdef WITH_COMPOWER
#include "powertrace.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define UDP_CLIENT_PORT 8765
#define UDP_SERVER_PORT 5678

#define DEBUG 1
#include "net/uip-debug.h"

#ifndef PERIOD
#define PERIOD 60
#endif

#define START_INTERVAL		(15 * CLOCK_SECOND)
#define SEND_INTERVAL		(PERIOD * CLOCK_SECOND)
#define SEND_TIME		(random_rand() % (SEND_INTERVAL))
#define MAX_PAYLOAD_LEN		300
#include "../../ctinMacro.h"

static const char *echoStr = "echo";
static const char *seqIDstr = "seqID";
#ifdef TELEKONT
#include "telekont.h"
static const char *idleTimeStr = "idleTime";
#endif //TELEKONT

#ifdef SLEEP_MODE
#include "sleep.h"
#endif //SLEEP_MODE

#ifdef RELAY
#include "relay.h"
static const char *relayStr = "relay";
#endif //RELAY

#ifdef THL
#include "sensor_light.h"
#include "sensor_humidity_temperature.h"
static const char *luxStr = "lux";
static const char *temperatureStr = "temperature";
static const char *humidityStr = "humidity";
#endif //THL

#ifdef WATER
#include "water.h"
static const char *waterSensorStateStr = "waterSensorState";
#endif //WATER

#ifdef PIR
#include "sensor_pir.h"
static const char *PIRsensorStateStr = "PIRsensorState";
#endif //PIR

#ifdef AIR
#include "sensor_air.h"
static const char *airSensorStateStr = "airSensorState";
#endif //AIR

#ifdef DIMMER
#include "dimmer.h"
static const char *dimmerPercentageStr = "dimmerPercentage";
#endif //DIMMER

#ifdef TWI_MODULE
#include "telekont_variables.h"
#include "i2c_master.h"
#include "access.h"
static const char *moduleStr = "twiModule";
#endif //TWI_MODULE

#define SKIP_SPEC_SYMB(result)	while(*result == ' ' || *result == '\t' || *result == '\"' || *result == '\'' || *result == ':' || *result == '=') result++;

static struct uip_udp_conn *client_conn;
static uip_ipaddr_t server_ipaddr;
extern process_event_t sendPacketEvent;
int32_t g_initialPacketId;
/*---------------------------------------------------------------------------*/
PROCESS(udp_client_process, "UDP client process");
AUTOSTART_PROCESSES(&udp_client_process, &connection_process);
/*---------------------------------------------------------------------------*/
static void
tcpip_handler(void)
{ 
	char *str;
	char *result;
	if(uip_newdata()) {
		str = uip_appdata;
		str[uip_datalen()] = '\0';
		PRINTF("DATA recv '%s'\n", str);
		
		result = strstr(str, echoStr);
		if(result)
		{
			result += strlen(echoStr);
			SKIP_SPEC_SYMB(result);
			if(*result >= '0' && *result <= '9')
			{
				int32_t tmp = atol(result);
				if(tmp == g_initialPacketId)
					setConnected(1);
			}
		}
	#ifdef TELEKONT
		result = strstr(str, idleTimeStr);
		if(result)
		{
			result += strlen(idleTimeStr);
			SKIP_SPEC_SYMB(result);
			if(*result >= '0' && *result <= '9')
			{
				int32_t tmp = atol(result);
				setIdleTime(tmp);
			}
		}
	#endif //TEKEKONT
	#ifdef RELAY
		{
			result = strstr(str, relayStr);
			if(result)
			{
				PRINTF("found command for relay: %s\n", relayStr);
				result += strlen(relayStr);
				SKIP_SPEC_SYMB(result);
				if(*result >= '0' && *result <= '9')
				{
					setAllrelay(atoi(result));
				}
			}
		}
		#endif //RELAY
		#ifdef DIMMER
		result = strstr(str, dimmerPercentageStr);
		if(result)
		{
			result += strlen(dimmerPercentageStr);
			SKIP_SPEC_SYMB(result);
			if(*result >= '0' && *result <= '9')
			{
				setDimmerPercentage(atoi(result));
			}
		}
		#endif //DIMMER 
		#ifdef TWI_MODULE
		{
			uint8_t slot;
			char buffer[16];
			for(slot = 0;  slot < MAX_SLOT;  slot++)
			{
				sprintf(buffer, "%s_%d", moduleStr, slot);
				result = strstr(str, buffer);
				if(result)
				{
					PRINTF("found command for twiModules: %s\n", buffer);
					result += strlen(buffer);

					SKIP_SPEC_SYMB(result);
					if(*result >= '0' && *result <= '9')
					{
						uint16_t value = atoi(result);
						SetDiskretChannels(slot,(value >> 8),value,0,0);
					}
				}
			}
		}
		#endif //TWI_MODULE
		result = strstr(str, seqIDstr);
		if(result)
		{
			uint32_t seqId = 0;
			PRINTF("%s detected\t", seqIDstr);
			result += strlen(seqIDstr);
			SKIP_SPEC_SYMB(result);
			if(*result >= '0' && *result <= '9')
			{
				seqId = atol(result);
				process_post(PROCESS_BROADCAST, sendPacketEvent, &seqId); //вход в сон происходит по приходу ответа от сервера.
			}
			PRINTF("seqID=%d\n", seqId);
		}
	}
}

/*---------------------------------------------------------------------------*/
static void
send_packet(void *ptr)
{
  char buf[MAX_PAYLOAD_LEN];
  //static int seq_id;
  //seq_id++;
  sprintf(buf, "{\n");
#ifdef TELEKONT
	sprintf(buf + strlen(buf), "\t\"%s\":%ld,\n", idleTimeStr, idleTime);
#endif //TELEKONT
#ifdef THL
	sprintf(buf + strlen(buf), "\t\"%s\":%d,\n\t\"%s\":%d,\n\t\"%s\":%d,\n", luxStr, g_lux, temperatureStr, g_temperature, humidityStr, g_humidity);
#endif //THL
#ifdef RELAY
	sprintf(buf + strlen(buf), "\t\"%s\": {\n", relayStr);
	sprintf(buf + strlen(buf), "\t\t\"chCount\": %d,\n", RELAY);
	sprintf(buf + strlen(buf), "\t\t\"value\": %d\n", getAllrelay());
	sprintf(buf + strlen(buf), "\t},\n");
#endif //RELAY
#ifdef WATER
	sprintf(buf + strlen(buf), "\t\"%s\":%d,\n", waterSensorStateStr, g_waterSensorData);
#endif //WATER
#ifdef PIR
	sprintf(buf + strlen(buf), "\t\"%s\":%d,\n", PIRsensorStateStr, getPIRsensorState());
#endif //PIR
#ifdef AIR
	sprintf(buf + strlen(buf), "\t\"%s\":%d,\n", airSensorStateStr, readAirSensor(0));
#endif //AIR
#ifdef DIMMER
	sprintf(buf + strlen(buf), "\t\"%s\":%d,\n", dimmerPercentageStr, getDimmerPercentage());
#endif //DIMMER
#ifdef TWI_MODULE
	{
		uint8_t slot;
		uint8_t buffer[2];
		uint8_t ret;
		consist_check();
		sprintf(buf + strlen(buf), "\t\"twiModules\": [\n", moduleStr, slot);

		for(slot = 0;  slot < MAX_SLOT;  slot++)
		{
			if(telekontConsist[slot][ADDR_TYPE]&MODULE_NOT_EXIST)
				continue;
			if(telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG)
				;
			else
			{
				sprintf(buf + strlen(buf), "\t{\n");
				ret=GetDiskretChannels(slot,buffer,buffer+1,0,0);
				if(!ret)
				{	
					sprintf(buf + strlen(buf), "\t\t\"id\": %d,\n", slot);
					sprintf(buf + strlen(buf), "\t\t\"chCount\": %d,\n", telekontConsist[slot][ADDR_CHANNELS]);
					sprintf(buf + strlen(buf), "\t\t\"value\": %d,\n", (buffer[0] << 8 | buffer[1]));
					sprintf(buf + strlen(buf), "\t\t\"type\":\"%s%s\"\n", telekontConsist[slot][ADDR_TYPE]&MODULE_IS_ANALOG ? "A" : "D",
						telekontConsist[slot][ADDR_TYPE]&MODULE_FOR_OUTPUT ? "O" : "I");  
				}
				sprintf(buf + strlen(buf), "\t},\n");
			}
		}
		sprintf(buf + strlen(buf), "\"end\"\n");
		sprintf(buf + strlen(buf), "\t],\n");

	}
#endif //TWI_MODULE
	if(ptr)
	{
		if(strstr(ptr, echoStr))
		{
			sprintf(buf + strlen(buf), "\t\"%s\":%s\n", echoStr, (char*)(ptr + 5));
		}
		else
		{ 
			sprintf(buf + strlen(buf), "\t\"%s\":%ld\n", seqIDstr, *((int32_t*)ptr));
		}
	}
	sprintf(buf + strlen(buf), "}");
	PRINTF("buffer ready, len: %d\n", strlen(buf));

	PRINTF("DATA send to %d: \n%s\n", server_ipaddr.u8[sizeof(server_ipaddr.u8) - 1], buf);
	uip_udp_packet_sendto(client_conn, buf, strlen(buf),
                        &server_ipaddr, UIP_HTONS(UDP_SERVER_PORT));
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Client IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
      /* hack to make address "final" */
      if (state == ADDR_TENTATIVE) {
	uip_ds6_if.addr_list[i].state = ADDR_PREFERRED;
      }
    }
  }
}
/*---------------------------------------------------------------------------*/
static void
set_global_address(void)
{
  uip_ipaddr_t ipaddr;

  uip_ip6addr(&ipaddr, 0xbbbb, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);

/* The choice of server address determines its 6LoPAN header compression.
 * (Our address will be compressed Mode 3 since it is derived from our link-local address)
 * Obviously the choice made here must also be selected in udp-server.c.
 *
 * For correct Wireshark decoding using a sniffer, add the /64 prefix to the 6LowPAN protocol preferences,
 * e.g. set Context 0 to aaaa::.  At present Wireshark copies Context/128 and then overwrites it.
 * (Setting Context 0 to aaaa::1111:2222:3333:4444 will report a 16 bit compressed address of aaaa::1111:22ff:fe33:xxxx)
 *
 * Note the IPCMV6 checksum verification depends on the correct uncompressed addresses.
 */
 
#if 1
/* Mode 1 - 64 bits inline */
   uip_ip6addr(&server_ipaddr, 0xbbbb, 0, 0, 0, 0, 0, 0, 1);
#elif 0
/* Mode 2 - 16 bits inline */
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0, 0x00ff, 0xfe00, 1);
#else
/* Mode 3 - derived from server link-local (MAC) address */
  uip_ip6addr(&server_ipaddr, 0xaaaa, 0, 0, 0, 0x0250, 0xc2ff, 0xfea8, 0xcd1a); //redbee-econotag
#endif
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_client_process, ev, data)
{
#if defined(SLEEP_MODE) || defined (TELEKONT)
#else
  static struct etimer periodic;
  static struct ctimer backoff_timer;
#endif //SLEEP_MODE || TELEKONT
#if WITH_COMPOWER
  static int print = 0;
#endif

  PROCESS_BEGIN();

  PROCESS_PAUSE();

  set_global_address();
  
  PRINTF("UDP client process started\n");

  print_local_addresses();

  /* new connection with remote host */
  client_conn = udp_new(NULL, UIP_HTONS(UDP_SERVER_PORT), NULL); 
  if(client_conn == NULL) {
    PRINTF("No UDP connection available, exiting the process!\n");
    PROCESS_EXIT();
  }
  udp_bind(client_conn, UIP_HTONS(UDP_CLIENT_PORT)); 

  PRINTF("Created a connection with the server ");
  PRINT6ADDR(&client_conn->ripaddr);
  PRINTF(" local/remote port %u/%u\n",
	UIP_HTONS(client_conn->lport), UIP_HTONS(client_conn->rport));

#if WITH_COMPOWER
  powertrace_sniff(POWERTRACE_ON);
#endif
#if defined(SLEEP_MODE) || defined (TELEKONT)
#else
  etimer_set(&periodic, SEND_INTERVAL);
#endif //SLEEP_MODE || TELEKONT
  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
	if(ev == sendPacketEvent) {
		send_packet(data);
	}
#if defined(SLEEP_MODE) || defined (TELEKONT)
#else
    if(etimer_expired(&periodic)) {
      etimer_reset(&periodic);
      ctimer_set(&backoff_timer, SEND_TIME, send_packet, NULL);

#if WITH_COMPOWER
      if (print == 0) {
	powertrace_print("#P");
      }
      if (++print == 3) {
	print = 0;
      }
#endif //WITH_COMPOWER
    }
#endif //SLEEP_MODE || TELEKONT
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
